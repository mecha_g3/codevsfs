#!/bin/bash
set -e
if [ $# -ne 1 ]; then
  echo "need name"
  exit 1
fi

name=$1
output_file="bench-$name-$(date +%Y%m%d%H%M).txt"
ls input/*.in | while read line; do
  echo "Benchmarking: $line"
  echo "!start" >> $output_file
  ./bin/main -bench chain -input $line -limit 10000 | tee -a $output_file
  echo "!end" >> $output_file
done
echo "Benchmark done"

