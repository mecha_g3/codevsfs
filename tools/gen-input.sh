#!/bin/bash
script_dir=$(cd $(dirname $0) && pwd)
cd $script_dir

for i in $(seq 10000 10299); do
  java -jar ./mod_client.jar gen-input -seed $i > $i.in
done

