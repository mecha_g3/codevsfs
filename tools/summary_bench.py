import sys
import math

def print_histgram(score_list, width = 50):
    if not score_list:
        return
    hist = {}
    min_index = 1 << 30
    max_index = 0
    for score in score_list:
        index = int(score + width / 2) / width
        if index not in hist:
            hist[index] = 0
            min_index = min(min_index, index)
            max_index = max(max_index, index)
        hist[index] += 1

    for i in xrange(min_index, max_index + 1):
        print "%04d-%04d" % (i * width, (i + 1) * width - 1),
        if i in hist:
            print "[]" * hist[i]
        else:
            print ""


def main():
    min_score = 1000000000
    max_score = 0
    total_score2 = 0
    total_score = 0
    total_time = 0
    total_sim = 0
    min_score_file = ""
    max_score_file = ""
    count = 0
    score_list = []

    with open(sys.argv[1]) as f:
        score = chain = time = sim = 0
        input_file = ""
        for line in f:
            line = line.strip()
            if line.startswith("!start"):
                score = chain = time = 0
            if line.startswith("!end"):
                total_score += score
                total_score2 += score * score
                total_time += time
                total_sim += sim
                score_list.append(score)
                if score < min_score:
                    min_score = score
                    min_score_file = input_file 
                if max_score < score:
                    max_score = score
                    max_score_file = input_file 
                score = chain = time = sim = 0
                input_file = ""
                count += 1
            if line.startswith("!score"):
                score = int(line.split(':', 1)[1])
            if line.startswith("!chain"):
                chain = int(line.split(':', 1)[1])
            if line.startswith("!time"):
                time = int(line.split(':', 1)[1])
            if line.startswith("!simulation"):
                sim = int(line.split(':', 1)[1])
            if line.startswith("!input"):
                input_file = line.split(':', 1)[1]

    if not count:
        print "No data"
        return

    avg_score = float(total_score) / count
    stdv_score = math.sqrt(total_score2 / count - avg_score * avg_score);

    print "=" * 20, "    SUMMARY   ", "=" * 20
    print "source", sys.argv[1]
    print "count", count
    print "min_score", min_score, "(", min_score_file, ")"
    print "max_score", max_score, "(", max_score_file, ")"
    print "avg_score", int(avg_score)
    print "stdv_score", int(stdv_score)
    print "2sigma(68%)", int(avg_score - stdv_score), "~", int(avg_score + stdv_score)
    print "=" * 20, "SCORE HISTGRAM", "=" * 20
    print_histgram(score_list, 50)
    return

if __name__ == '__main__':
    main()
