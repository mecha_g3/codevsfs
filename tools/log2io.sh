#!/bin/bash
script_dir=$(cd $(dirname $0) && pwd)

for json in "$@"; do
  java -jar $script_dir/mod_client.jar log2io -json $json
done

