package main

import (
	"io"
	"io/ioutil"
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"
)

var (
	baseurl                = "http://52.198.238.77/codevsforstudent/log"
	basedir                = "/tmp"
)

func usage() {
	log.Fatalf("Usage: %s path/to/download_dir", os.Args[0])
}

func main() {
	if len(os.Args) < 2 {
		usage()
	}
	basedir = os.Args[1]

	err := get()
	if err != nil {
		log.Println(err)
	}
}

func get() error {
	res, err := http.Get(baseurl)
	if err != nil {
		return err
	}
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}
	lines := strings.Split(string(body), "\n")
	for _, line := range lines {
		if !strings.Contains(line, "href") {
			continue
		}
		a := strings.Split(line, "<")
		b := strings.Split(a[len(a)-2], ">")
		fileName := b[len(b)-1]
		err = downloadIfNotExist(fileName)
		if err != nil {
			return err
		}
	}
	return nil
}

func downloadIfNotExist(fileName string) error {
	dstPath := filepath.Join(basedir, fileName)
	_, err := os.Stat(dstPath)
	if err == nil {
		log.Println("Skip  Download", fileName)
		return nil
	}
	time.Sleep(time.Second)
	log.Println("Start Download", fileName)

	res, err := http.Get(baseurl + "/" + fileName)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	log.Println(res.Status)
	if res.StatusCode != 200 {
		return fmt.Errorf(res.Status)
	}

	f, err := ioutil.TempFile(basedir, "downloading")
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = io.Copy(f, res.Body)
	if err != nil {
		return err
	}

	err = os.Rename(f.Name(), dstPath )
	if err != nil {
		return err
	}

	return nil
}
