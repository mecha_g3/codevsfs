#!/bin/bash
set -e
ls logs/*.in | while read line; do
  echo "Testing: $line"
  ./bin/main -test -simtest ${line%_*}_
done
echo "Test Passed"

