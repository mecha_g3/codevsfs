package com.example;

import contest.game.GameResult;
import contest.game.GameInfo;
import contest.game.GameSimulator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.err.println("need command name");
            return;
        }

        String cmd = args[0];
        Map<String, String> argMap = new HashMap<>();

        String prev = "";
        for (int i = 1; i < args.length; ++i) {
            if (args[i].startsWith("-")) {
                argMap.put(args[i], "");
                prev = args[i];
            } else if (!prev.isEmpty()) {
                String tmp = argMap.get(prev);
                argMap.put(prev, tmp.isEmpty() ? args[i] : tmp + " " + args[i]);
            }
        }

        switch (cmd) {
            case "gen-input":
                generateInput(argMap);
                break;
            case "log2io":
                log2io(argMap);
                break;
            default:
                System.err.println("no such command");
        }
    }

    public static void generateInput(Map<String, String> argMap) {
        int seed = Integer.parseInt(argMap.get("-seed"));
        GameSimulator gs = new GameSimulator(seed, false);
        String input0 = gs.toInputString(0, 180000);
        System.out.print(input0);
    }

    private static void save(String content, String fileNameToSave) {
        try {
            BufferedWriter f = new BufferedWriter(new FileWriter(new File(fileNameToSave)));
            f.write(content);
            f.flush();
            f.close();
        } catch (IOException e) {
            System.out.println("Failed to save");
            e.printStackTrace();
        }
    }

    public static void log2io(Map<String, String> argMap) {
        String jsonFile = argMap.get("-json");
        StringBuffer input0buf = new StringBuffer();
        StringBuffer input1buf = new StringBuffer();
        StringBuffer output0buf = new StringBuffer();
        StringBuffer output1buf = new StringBuffer();

        GameResult result = GameResult.readFileJSON(jsonFile);
        GameInfo gi = new GameInfo(result.getGameConfig(), result.getAI0(), result.getAI1(), result.allBattle, result.gameSeed);
        gi.init();

        int stageCount = 0;
        GameSimulator simulator = new GameSimulator(result.gameSeed);
        long restThinkNanoSec0 = 180000000000L;
        long restThinkNanoSec1 = 180000000000L;
        boolean isEnd = false;
        int endIndex = Math.min(result.thinkTime0.length, result.thinkTime1.length);

        for (int idx = 0; idx < endIndex && !isEnd; ++idx) {
            String out0 = gi.ai0.think(null);
            String out1 = gi.ai1.think(null);
            if (out0 == null || out1 == null) break;

            long thinkTimeLeft0 = restThinkNanoSec0 / 1000000L;
            long thinkTimeLeft1 = restThinkNanoSec1 / 1000000L;
            simulator.setThinkTimeLeft(thinkTimeLeft0, thinkTimeLeft1);
            String in0 = simulator.toInputString(0, thinkTimeLeft0);
            String in1 = simulator.toInputString(1, thinkTimeLeft1);
            simulator.step(out0, out1);

            input0buf.append(in0);
            input1buf.append(in1);
            output0buf.append(out0);
            output1buf.append(out1);

            restThinkNanoSec0 -= result.thinkTime0[idx];
            restThinkNanoSec1 -= result.thinkTime1[idx];

            boolean ai0gameTimeout = restThinkNanoSec0 < 0L;
            boolean ai1gameTimeout = restThinkNanoSec1 < 0L;
            boolean ai0stepTimeout = false;
            boolean ai1stepTimeout = false;

            if(result.isOnline) {
                ai0stepTimeout = result.thinkTime0[idx] / 1000000L > 20000L;
                ai1stepTimeout = result.thinkTime1[idx] / 1000000L > 20000L;
            }

            if(simulator.end() || ai0gameTimeout || ai1gameTimeout || ai0stepTimeout || ai1stepTimeout) {
                save(input0buf.toString(), jsonFile + String.format("%02d_0.in", stageCount));
                save(input1buf.toString(), jsonFile + String.format("%02d_1.in", stageCount));
                save(output0buf.toString(), jsonFile + String.format("%02d_0.out", stageCount));
                save(output1buf.toString(), jsonFile + String.format("%02d_1.out", stageCount));
                input0buf.setLength(0);
                input1buf.setLength(0);
                output0buf.setLength(0);
                output1buf.setLength(0);

                ++stageCount;
                isEnd = ++stageCount >= gi.getAllBattle();
                simulator.init(gi.gameSeed + (long) stageCount, false);
                restThinkNanoSec0 = 300000000000L;
                restThinkNanoSec1 = 300000000000L;
            }
        }
    }
}
