all: build

.PHONY: deploy
deploy:
	rm -f deploy.zip
	zip -r deploy.zip Makefile src run.sh

.PHONY: prod
prod:
	mkdir -p bin
	g++ -pthread -march=haswell -DNDEBUG -std=c++14 src/main.cpp -Ofast -o bin/main

.PHONY: build
build:
	mkdir -p bin
	g++ -pthread -march=haswell -std=c++11 src/main.cpp -O3 -g -o bin/main

.PHONY: vis
vis:
	mkdir -p bin
	g++ -pthread -DENABLE_GV $(shell sdl2-config --cflags --libs) -lSDL2_ttf -framework OpenGL -std=c++11 src/main.cpp -O3 -o bin/main

.PHONY: debug
debug:
	mkdir -p bin
	g++ -pthread -std=c++11 src/main.cpp -O0 -g3 -o bin/main


.PHONY: format
format:
	bash format.sh

.PHONY: tag
tag:
	ctags -R --languages=c++
