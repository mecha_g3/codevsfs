#pragma once
#include <vector>

#include "common.hpp"

struct BenchResult {
  std::vector<Command> commands;
  int chain = 0;
  int score = 0;
};
