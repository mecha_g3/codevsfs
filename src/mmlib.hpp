#pragma once

#include <array>
#include <cassert>
#include <initializer_list>
#include <iostream>
#include <list>
#include <mutex>
#include <set>

namespace mmlib {

template <typename T1, typename T2>
struct Pair {
  Pair() {}
  Pair(const Pair&) = default;
  Pair(Pair&&) = default;
  Pair& operator=(const Pair&) = default;
  Pair(const T1& f, const T2& s) : first(f), second(s) {}
  Pair(T1&& f, T2&& s) : first(f), second(s) {}

  T1 first;
  T2 second;
};

template <typename T, unsigned int N>
class Vector {
 public:
  template <class Itr>
  void assign(Itr first, Itr last) {
    size_ = 0;
    while (first != last) {
      assert(size_ < N);
      m_[size_++] = *first++;
    }
  }

  void assign(size_t n, const T& v) {
    assert(n < N);
    size_ = n;
    while (n--) m_[n] = v;
  }

  void assign(std::initializer_list<T> il) { assign(il.begin(), il.end()); }

  void resize(size_t n) {
    assert(0 <= n);
    assert(n < N);
    size_ = n;
  }

  T* begin() { return m_.begin(); };
  const T* begin() const { return m_.begin(); };

  T* end() { return m_.begin() + size_; };
  const T* end() const { return m_.begin() + size_; };

  void clear() { size_ = 0; };

  size_t size() const { return size_; };

  void push_back(const T& v) {
    assert(size_ < N);
    m_[size_++] = v;
  }

  void pop_back() {
    assert(size_ != 0);
    size_--;
  }

  T& front() {
    assert(0 < size_);
    return *m_.begin();
  }

  const T& front() const {
    assert(0 < size_);
    return *m_.begin();
  }

  T& back() {
    assert(0 < size_);
    return *(m_.begin() + size_ - 1);
  }

  const T& back() const {
    assert(0 < size_);
    return *(m_.begin() + size_ - 1);
  }

  T& operator[](size_t pos) {
    assert(pos < size_);
    return m_[pos];
  }

  const T& operator[](size_t pos) const {
    assert(pos < size_);
    return m_[pos];
  }

 private:
  std::array<T, N> m_;
  uint32_t size_ = 0;
};

template <typename T, int N, class Compare = std::less<T> >
class Ranking {
 public:
  void on_rankout(std::function<void(const T&)> op) { on_pop_min_ = op; }

  bool is_rankin(const T& value) {
    return m_.size() < N ? true : Compare()(min_element(), value);
  }

  void push(const T& value) {
    m_.insert(value);
    if (size() == N) {
      on_pop_min_(min_element());
      pop_min();
    }
  }

  T min_element() {
    assert(!empty());
    return *m_.begin();
  }

  const T& min_element() const {
    assert(!empty());
    return *m_.begin();
  }

  T max_element() {
    assert(!empty());
    return *m_.rbegin();
  }

  const T& max_element() const {
    assert(!empty());
    return *m_.rbegin();
  }

  void pop_min() {
    assert(!empty());
    m_.erase(m_.begin());
  }

  void pop_max() {
    assert(!empty());
    m_.erase(--m_.end());
  }

  bool empty() const { return m_.empty(); }

  size_t size() const { return m_.size(); }

  void clear() { m_.clear(); }

  const std::multiset<T>& data() const { return m_; }

 private:
  std::multiset<T> m_;
  std::function<void(const T&)> on_pop_min_;
};

template <typename T, int AllocCount = 32768>
class MemoryPool {
 public:
  T* get() {
    std::lock_guard<std::mutex> lock(mtx_);
    if (pool_.empty()) {
      alloc_count_++;
      auto ptr = std::malloc(AllocCount * sizeof(T));
      assert(ptr != nullptr);

      T* buf = reinterpret_cast<T*>(ptr);
      for (int i = 0; i < AllocCount; ++i) {
        assert(&buf[i] != nullptr);
        pool_.push_back(&buf[i]);
      }
    }
    get_count_++;
    auto ret = pool_.back();
    assert(ret != nullptr);
    pool_.pop_back();
    return ret;
  }

  void put(T* used) {
    assert(used != nullptr);
    std::lock_guard<std::mutex> lock(mtx_);
    pool_.push_back(used);
    put_count_++;
  }

  int get_count() {
    std::lock_guard<std::mutex> lock(mtx_);
    return get_count_;
  }

  int put_count() {
    std::lock_guard<std::mutex> lock(mtx_);
    return put_count_;
  }

  int alloc_count() {
    std::lock_guard<std::mutex> lock(mtx_);
    return alloc_count_;
  }

  void print_stat() {
    std::lock_guard<std::mutex> lock(mtx_);
    std::cerr << "===== POOL STAT =====" << std::endl;
    std::cerr << "pool.get_count:" << get_count_ << std::endl;
    std::cerr << "pool.put_count:" << put_count_ << std::endl;
    std::cerr << "pool.alloc_count:" << alloc_count_ << std::endl;
    std::cerr << "=====================" << std::endl;
  }

  MemoryPool() = default;
  MemoryPool(const MemoryPool&) = delete;
  const MemoryPool& operator=(const MemoryPool&) = delete;

 private:
  int get_count_ = 0;
  int put_count_ = 0;
  int alloc_count_ = 0;
  std::list<T*> pool_;
  std::mutex mtx_;
};

}  // namespace mmlib
