#pragma once

#include "benchmark_misc.hpp"
#include "multi_chokudai_search.hpp"

static BenchResult ImplMultiChokudaiSearchBenchmark(
    const GameInfo &game_info, const TurnInfo &turn_info, int time_limit_ms,
    std::map<std::string, std::string> &args) {
  static const int BeamWidth = 6000;
  static const int BeamDepth = 60;

  const auto &packs = game_info.packs;

  BenchResult result;
  Board board = turn_info.m_board;

  int target_turn = 20;
  int target_range = 3;
  if (args.find("-target_turn") != args.end()) {
    target_turn = std::stoi(args["-target_turn"]);
  }

  using Search = MultiChokudaiSearch<BeamWidth, BeamDepth>;
  Search search;
  search.Reset(packs, board, turn_info.turn, turn_info.m_stock);

  std::vector<EvalFuncMyBest> eval_funcs(3);

  for (int t = 0; t < target_range; ++t) {
    eval_funcs[t].Init();
    auto pack = packs.Get(target_turn + t - 1);
    for (int rot = 0; rot < 4; ++rot) {
      for (int pos = -2; pos <= 0; ++pos) {
        if (eval_funcs[t].AddTargetPack(pack, pos, rot)) {
          break;
        }
      }
      for (int pos = kWidth - 1; kWidth - 3 <= pos; --pos) {
        if (eval_funcs[t].AddTargetPack(pack, pos, rot)) {
          break;
        }
      }
    }
  }

  search.Run(eval_funcs, target_turn + target_range, time_limit_ms);

  auto report = search.GetReports();
  if (report.empty()) {
    std::cerr << "report is empty" << std::endl;
    return result;
  }

  std::sort(report.rbegin(), report.rend(),
            [](const Search::Report &a, const Search::Report &b) {
              return std::pow(0.9, a.log.size() + 1) * a.score <
                     std::pow(0.9, b.log.size() + 1) * b.score;
            });
  auto best = report.front();

  result.chain = best.chain;
  result.score = best.score;

  for (const auto &r : best.log) {
    result.commands.push_back(r);
  }
  result.commands.push_back(best.cmd);

  search.ClearReports();

  std::cerr << "ranking:" << std::endl;
  for (int i = 0; i < std::min<int>(10, report.size()); ++i) {
    std::cerr << "rank:" << i + 1 << " turn:" << report[i].log.size() + 1
              << " score:" << report[i].score << " chain:" << report[i].chain
              << std::endl;
  }

  return result;
}

static void RunMultiChokudaiSearchBenchmark(
    const GameInfo &game_info, const TurnInfo &turn_info, int time_limit_ms,
    std::map<std::string, std::string> &args) {
  std::cout << "!start" << std::endl;
  auto start_time = std::chrono::system_clock::now();
  auto result = ImplMultiChokudaiSearchBenchmark(game_info, turn_info,
                                                 time_limit_ms, args);
  auto end_time = std::chrono::system_clock::now();
  auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(
                     end_time - start_time)
                     .count();

  std::cout << "!time_limit:" << time_limit_ms << std::endl;
  std::cout << "!time:" << (int)elapsed << std::endl;
  std::cout << "!score:" << result.score << std::endl;
  std::cout << "!chain:" << result.chain << std::endl;
  std::cout << "!turn:" << result.commands.size() << std::endl;
  std::cout << "!simulation:" << g_metrics.simulator_fall_count << std::endl;
  board_pool.print_stat();
  g_metrics.Print();
  std::cout << "!end" << std::endl;
}
