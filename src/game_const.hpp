#pragma once

static constexpr int kWall = 11;
static constexpr int kTurn = 500;
static constexpr int kSum = 10;
static constexpr int kPackSize = 3;
static constexpr int kWidth = 10;
static constexpr int kHeight = 20;
