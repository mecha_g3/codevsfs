#pragma once

#include <algorithm>
#include <deque>
#include <functional>
#include <iostream>
#include <vector>

#include "battle_log.hpp"
#include "chokudai_search.hpp"
#include "common.hpp"
#include "eval_func.hpp"

class MechaAI {
 public:
  using Chokudai = ChokudaiSearch<10000, 30>;

  class TimeManager {
   public:
    void Update(const TurnInfo &turn_info) { turn_info_ = turn_info; }

    int ThinkCounterTime() {
      if (90 * 1000 < turn_info_.remaining_ms) {
        return 18 * 1000;
      } else if (30 * 1000 < turn_info_.remaining_ms) {
        return 5 * 1000;
      }
      return 1 * 1000;
    }

    int ThinkChainTime() {
      if (turn_info_.turn == 0) {
        return 18 * 1000;
      }
      if (90 * 1000 < turn_info_.remaining_ms) {
        return 10 * 1000;
      } else if (30 * 1000 < turn_info_.remaining_ms) {
        return 5 * 1000;
      }
      return 1 * 1000;
    }

   private:
    TurnInfo turn_info_;
  };

  void UseBattleLog() {
    log_holder_.Load();
    std::cerr << "battle_log_size:" << log_holder_.battle_log_list.size()
              << std::endl;
    if (log_holder_.battle_log_list.empty()) {
      std::cerr << "battle log is empty." << std::endl;
      return;
    }
    auto last = log_holder_.battle_log_list.back();
    if (last.broken) {
      std::cerr << "battle log is broken." << std::endl;
      return;
    }
    auto elapsed = (std::time(nullptr) - last.start_time);
    std::cerr << "the last battle log is before " << elapsed << "seconds."
              << std::endl;
    if (elapsed > 180) {
      std::cerr << "battle log is old." << std::endl;
      return;
    }
    if (last.turn_info_list.size() < 30) {
      std::cerr << "too short log" << std::endl;
      return;
    }

    std::cerr << "target:" << last.target_first_attack_turn << std::endl;
    int m_first_attack_turn = 0;
    int m_first_attack_stock = 0;
    int o_first_attack_turn = 0;
    int o_first_attack_stock = 0;
    for (int t = 0; t < last.turn_info_list.size(); ++t) {
      const auto &ti = last.turn_info_list[t];
      if (m_first_attack_turn == 0 && ti.o_stock) {
        m_first_attack_turn = t;
        m_first_attack_stock = ti.o_stock;
      }
      if (o_first_attack_turn == 0 && ti.m_stock) {
        o_first_attack_turn = t;
        o_first_attack_stock = ti.m_stock;
      }
    }
    if (m_first_attack_stock == 0 && o_first_attack_stock == 0) {
      std::cerr << "no attack." << std::endl;
      return;
    }
    auto m_last_board = last.turn_info_list.back().m_board;
    auto o_last_board = last.turn_info_list.back().o_board;
    std::cerr << "m_last_board" << std::endl;
    PrintBoard(m_last_board, std::cerr);
    std::cerr << "o_last_board" << std::endl;
    PrintBoard(o_last_board, std::cerr);

    bool probably_lose = o_last_board.EmptyCount() > m_last_board.EmptyCount();
    if (probably_lose) {
      std::cerr << "probably lose" << std::endl;
      std::cerr << "m" << m_first_attack_turn << std::endl;
      std::cerr << "o" << o_first_attack_turn << std::endl;
      if (o_first_attack_turn < m_first_attack_turn) {
        if (15 < o_first_attack_turn && o_first_attack_turn < 30) {
          first_attack_turn_ = o_first_attack_turn - 1;
        }
      } else if (o_first_attack_turn > m_first_attack_turn) {
        if (15 < m_first_attack_turn && m_first_attack_turn < 30) {
          first_attack_turn_ = m_first_attack_turn + 1;
        }
      }
    } else {
      std::cerr << "probably win" << std::endl;
      if (15 < last.target_first_attack_turn &&
          last.target_first_attack_turn < 30) {
        std::cerr << "use target fire:" << last.target_first_attack_turn
                  << std::endl;

        first_attack_turn_ = last.target_first_attack_turn;
      }
    }
    std::cerr << "next first attack turn:" << first_attack_turn_ << std::endl;
  }

  std::string Name() const { return "mecha_g3"; }

  bool Think(std::istream &is, std::ostream &os) {
    Input(is);
    auto met = CalcTurnMetrics(game_info_, turn_info_);

    turn_info_.Print();
    met.Print();

    if (turn_info_.turn == 0) {
      ThinkFirstAttack();
      board_pool.print_stat();
      DoBuffered(os);
      return true;
    }

    bool come_jama = expected_m_stock_ != turn_info_.m_stock;
    if (!come_jama && !cmd_buf_.empty()) {
      StopBackgroundIfRunning();
      UpdateBestCommand();
      int depth = turn_info_.turn - current_best_base_turn_;
      chokudai_.Determined(depth, cmd_buf_.front());
      ContinueBackground();
      DoBuffered(os);
      return true;
    }

    bool attack_done = cmd_buf_.empty();
    cmd_buf_.clear();

    ResetChokudai();

    if (attack_done) {
      ThinkChain(fire_interval_);
    } else {
      ThinkCounter(fire_interval_);
    }

    if (!cmd_buf_.empty()) {
      DoBuffered(os);
      return true;
    }

    std::cerr << "GAME OVER :(" << std::endl;
    os << "50 50" << std::endl;
    return false;
  }

  void SetGameInfo(const GameInfo &gi) {
    first_read_ = true;
    current_.Clear();
    game_info_ = gi;
    packs_ = game_info_.packs;
    gp_holder_.Init(packs_);
  }

  void UpdateTurnInfo(const TurnInfo &ti) {
    turn_info_ = ti;
    current_ = turn_info_.m_board;
    timeman_.Update(turn_info_);
  }

  std::deque<Command> GetBuffer() { return cmd_buf_; }

  bool ThinkFirstAttack() { return ThinkChain(first_attack_turn_); }

  void ContinueBackground() {
    StopBackgroundIfRunning();
    chokudai_.RunBackground(background_eval_func_, background_depth_);
  }

  void RunBackground(EvalFuncMyBest eval_func, int depth) {
    StopBackgroundIfRunning();
    background_eval_func_ = eval_func;
    background_depth_ = depth;
    chokudai_.RunBackground(background_eval_func_, background_depth_);
  }

  void StopBackgroundIfRunning() {
    if (chokudai_.IsRunning()) {
      chokudai_.StopBackground();
    }
  }

  void UpdateBestCommand() {
    auto report = chokudai_.GetReports();
    std::sort(report.rbegin(), report.rend(),
              [](const Chokudai::Report &a, const Chokudai::Report &b) {
                return std::pow(0.9, a.log.size() + 1) * a.score <
                       std::pow(0.9, b.log.size() + 1) * b.score;
              });
    auto best = report.front();
    if (current_best_.score < best.score) {
      std::cerr << "update best" << std::endl;
      std::cerr << current_best_.score << "->" << best.score << std::endl;
      int index = turn_info_.turn - current_best_base_turn_;
      std::cerr << "before buf size:" << cmd_buf_.size() << std::endl;
      cmd_buf_.clear();
      for (int i = index; i < best.log.size(); ++i) {
        cmd_buf_.push_back(best.log[i]);
      }
      cmd_buf_.push_back(best.cmd);
      current_best_ = best;
      std::cerr << "after buf size:" << cmd_buf_.size() << std::endl;
    }
  }

  bool ThinkChain(int depth) {
    ResetChokudai();

    EvalFuncMyBest eval_func;
    auto target_turn = turn_info_.turn + depth;
    eval_func.Init();
    auto pack = packs_.Get(target_turn);
    eval_func.AddTargetPack(pack);

    chokudai_.Run(eval_func, depth + 1, timeman_.ThinkChainTime());

    auto report = chokudai_.GetReports();
    if (report.empty()) return false;

    std::sort(report.rbegin(), report.rend(),
              [](const Chokudai::Report &a, const Chokudai::Report &b) {
                return std::pow(0.9, a.log.size() + 1) * a.score <
                       std::pow(0.9, b.log.size() + 1) * b.score;
              });

    auto best = report.front();

    std::cerr << "think done." << std::endl;
    std::cerr << "expected score:" << best.score << " chain:" << best.chain
              << std::endl;
    std::cerr << "ranking:" << std::endl;
    for (int i = 0; i < std::min<int>(10, report.size()); ++i) {
      std::cerr << "rank:" << i + 1 << " turn:" << report[i].log.size() + 1
                << " score:" << report[i].score << " chain:" << report[i].chain
                << std::endl;
    }

    for (int i = 0; i < best.log.size(); ++i) {
      cmd_buf_.push_back(best.log[i]);
    }
    cmd_buf_.push_back(best.cmd);

    current_best_ = best;
    current_best_base_turn_ = turn_info_.turn;
    chokudai_.Determined(0, cmd_buf_.front());
    RunBackground(eval_func, depth + 1);
    return true;
  }

  bool ThinkCounter(int depth) {
    ResetChokudai();
    EvalFuncMyBest eval_func;
    eval_func.Init();
    for (int num = 1; num < 10; ++num) {
      for (int pos = 0; pos < 3; ++pos) {
        eval_func.AddCheckPack(pos, num);
      }
      for (int pos = 7; pos < 10; ++pos) {
        eval_func.AddCheckPack(pos, num);
      }
    }
    chokudai_.Run(eval_func, depth, timeman_.ThinkCounterTime());
    auto report = chokudai_.GetReports();
    if (report.empty()) return false;
    std::sort(report.rbegin(), report.rend(), [](const Chokudai::Report &a,
                                                 const Chokudai::Report &b) {
      return std::pow(0.95, a.log.size() + 1) * a.score - a.after_stock <
             std::pow(0.95, b.log.size() + 1) * b.score - b.after_stock;
    });
    auto best = report.front();
    std::cerr << "--- counter think done" << std::endl;
    for (int i = 0; i < std::min<int>(30, report.size()); ++i) {
      std::cerr << "rank:" << i + 1 << " turn:" << report[i].log.size() + 1
                << " score:" << report[i].score << " chain:" << report[i].chain
                << std::endl;
    }

    for (int i = 0; i < best.log.size(); ++i) {
      cmd_buf_.push_back(best.log[i]);
    }
    cmd_buf_.push_back(best.cmd);

    current_best_ = best;
    current_best_base_turn_ = turn_info_.turn;
    chokudai_.Determined(0, cmd_buf_.front());
    RunBackground(eval_func, depth);
    return true;
  }

 private:
  void Input(std::istream &is) {
    if (!first_read_) {
      first_read_ = true;
      current_.Clear();
      game_info_ = ReadGameInfo(is);
      packs_ = game_info_.packs;
      gp_holder_.Init(packs_);
      log_writer_.WriteBegin(game_info_, first_attack_turn_);
    }
    turn_info_ = ReadTurnInfo(is);
    current_ = turn_info_.m_board;
    timeman_.Update(turn_info_);
    log_writer_.Write(turn_info_);
  }

  void ResetChokudai() {
    if (chokudai_.IsRunning()) {
      chokudai_.StopBackground();
    }
    chokudai_.Reset(packs_, current_, turn_info_.turn, turn_info_.m_stock);
  }

  void DoBuffered(std::ostream &os) {
    auto cmd = cmd_buf_.front();
    cmd_buf_.pop_front();
    UpdateCurrent(cmd);
    os << cmd.GetPos() << " " << cmd.GetRot() << std::endl;
  }

  void UpdateCurrent(Command cmd) {
    Simulator sim;
    auto tmp_board = Clone(&current_);
    auto conv_pack = ConvertPack(packs_.Get(turn_info_.turn), cmd.GetRot(),
                                 turn_info_.m_stock);
    if (sim.Fall(current_, *tmp_board, conv_pack.first, cmd.GetPos())) {
      expected_m_stock_ = std::max(0, conv_pack.second - sim.last_score() / 5);
    } else {
      std::cerr << "[warn] invalid command decided." << std::endl;
    }
    board_pool.put(tmp_board);
  }

  int first_attack_turn_ = 21;
  int fire_interval_ = 19;
  int expected_m_stock_ = 0;
  bool first_read_ = false;

  TimeManager timeman_;
  TurnInfo turn_info_;
  GameInfo game_info_;
  GoodPackHolder gp_holder_;
  PackMan packs_;
  Board current_;
  std::deque<Command> cmd_buf_;
  Chokudai chokudai_;
  BattleLogHolder log_holder_;
  BattleLogWriter log_writer_;
  Chokudai::Report current_best_;
  int current_best_base_turn_ = 0;
  int background_depth_ = 0;
  EvalFuncMyBest background_eval_func_;
};
