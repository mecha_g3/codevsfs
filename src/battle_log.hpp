#pragma once

#include <ctime>
#include <fstream>
#include <string>
#include <vector>
#include "common.hpp"

struct BattleLog {
  std::time_t start_time;
  PackMan packs;
  std::vector<TurnInfo> turn_info_list;
  int target_first_attack_turn;
  bool broken;
};

class BattleLogHolder {
 public:
  void Load() {
    std::cerr << "log load start" << std::endl;
    std::ifstream ifs("last_battle_log.txt");
    std::string line;
    BattleLog battle_log;
    TurnInfo turn_info;

    int begin_count = 0;
    int turn_count = 0;

    bool first_read = false;
    battle_log.broken = false;

    while (ifs >> line) {
      if (line == "!begin") {
        begin_count++;
        if (!first_read) {
          first_read = true;
        } else {
          battle_log_list.push_back(battle_log);
        }
        battle_log.broken = false;

        std::time_t time;
        ifs >> time;

        int first_attack_turn = 0;
        ifs >> first_attack_turn;

        PackMan pm;
        Pack p;
        for (int i = 0; i < 300; ++i) {
          p.Clear();
          int k;
          for (int y = 0; y < 3; ++y) {
            for (int x = 0; x < 3; ++x) {
              ifs >> k;
              p.Set(x, y, k);
            }
          }
          pm.Set(i, p);
        }
        battle_log.target_first_attack_turn = first_attack_turn;
        battle_log.start_time = time;
        battle_log.packs = pm;
      } else if (line == "!turn") {
        turn_count++;
        ifs >> turn_info.turn;
        ifs >> turn_info.remaining_ms;
        ifs >> turn_info.m_stock;
        ifs >> turn_info.o_stock;
        int k = 0;
        for (int y = 15; y >= 0; --y) {
          for (int x = 0; x < 10; ++x) {
            ifs >> k;
            turn_info.m_board.Set(x, y, k);
          }
        }
        for (int y = 15; y >= 0; --y) {
          for (int x = 0; x < 10; ++x) {
            ifs >> k;
            turn_info.o_board.Set(x, y, k);
          }
        }
        battle_log.turn_info_list.push_back(turn_info);
      } else {
        std::cerr << "[warn]" << std::endl;
        std::cerr << "begin_count:" << begin_count << std::endl;
        std::cerr << "turn_count:" << turn_count << std::endl;
        std::cerr << "??" << line << std::endl;
        battle_log.broken = true;
      }
    }

    std::cerr << "begin_count:" << begin_count << std::endl;
    std::cerr << "turn_count:" << turn_count << std::endl;

    if (first_read) {
      battle_log_list.push_back(battle_log);
      std::cerr << "battle log loaded : " << battle_log_list.size()
                << std::endl;
    }
  }

  std::vector<BattleLog> battle_log_list;
};

class BattleLogWriter {
 public:
  void WriteBegin(const GameInfo& game_info, int first_attack_turn) {
    ofs.open("last_battle_log.txt", std::ofstream::out | std::ofstream::app);
    if (!ofs.is_open()) {
      std::cerr << "failed to write battle log." << std::endl;
      return;
    }

    std::time_t t = std::time(nullptr);
    ofs << "!begin" << std::endl;
    ofs << t << std::endl;
    ofs << first_attack_turn << std::endl;
    for (int i = 0; i < 300; ++i) {
      const auto pack = game_info.packs.Get(i);
      for (int y = 0; y < 3; ++y) {
        for (int x = 0; x < 3; ++x) {
          ofs << pack.Get(x, y);
          if (x != 2)
            ofs << " ";
          else
            ofs << std::endl;
        }
      }
    }
  }

  void Write(const TurnInfo& ti) {
    ofs << "!turn" << std::endl;
    ofs << ti.turn << std::endl;
    ofs << ti.remaining_ms << std::endl;
    ofs << ti.m_stock << std::endl;
    ofs << ti.o_stock << std::endl;
    for (int y = 15; y >= 0; --y) {
      for (int x = 0; x < 10; ++x) {
        ofs << ti.m_board.Get(x, y);
        if (x != 9)
          ofs << " ";
        else
          ofs << std::endl;
      }
    }
    for (int y = 15; y >= 0; --y) {
      for (int x = 0; x < 10; ++x) {
        ofs << ti.o_board.Get(x, y);
        if (x != 9)
          ofs << " ";
        else
          ofs << std::endl;
      }
    }
  }

 private:
  std::ofstream ofs;
};
