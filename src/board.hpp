#pragma once

#include <algorithm>
#include <array>
#include <cassert>
#include <string>

#include "game_const.hpp"
#include "immintrin.h"

class FastBoard {
 public:
  FastBoard() { Clear(); };
  FastBoard(const std::string str) {
    assert(0 < str.length());
    assert(str.length() % 10 == 0);

    Clear();
    const int max_y = str.length() / 10 - 1;
    for (int i = 0; i < str.length(); ++i) {
      const char c = str[i];
      const int y = max_y - i / 10;
      const int x = i % 10;
      if (c == ' ')
        Set(x, y, 0);
      else if (c == '#')
        Set(x, y, 11);
      else if ('0' <= c && c <= '9')
        Set(x, y, c - '0');
      else
        assert(false);
    }
  }

  uint64_t Get(int x, int y) const {
    assert(0 <= x && x < 10);
    assert(0 <= y && y < 16);
    return (m_[x] >> (y << 2)) & 0x0FULL;
  }

  uint64_t Get(int x) const { return m_[x]; }

  void Set(int x, int y, int v) {
    assert(0 <= x && x < 10);
    assert(0 <= y && y < 16);
    assert(0 <= v && v <= 11 && v != 10);
    const uint64_t u = Get(x, y);
    if (u) size_--;
    if (u == kWall) jama_count_--;
    if (v) size_++;
    if (v == kWall) jama_count_++;
    m_[x] ^= (u ^ v) << (y << 2);
  }

  void Shrink(int x, uint64_t erased_y_mask) {
    size_ -= _mm_popcnt_u64(erased_y_mask) / 4;
    assert(0 <= size_ && size_ <= 160);
    m_[x] = _pext_u64(m_[x], ~erased_y_mask);
  }

  int Height(int x) const { return 16 - _lzcnt_u64(m_[x]) / 4; }

  void Clear() {
    m_.fill(0);
    size_ = 0;
    jama_count_ = 0;
  }

  int EmptyCount() const { return 10 * 16 - TotalBlockCount(); }
  int NormalBlockCount() const { return TotalBlockCount() - JamaBlockCount(); }
  int JamaBlockCount() const { return jama_count_; }
  int TotalBlockCount() const { return size_; }
  uint64_t Hash() const {
    uint64_t ret = 37;
    for (int i = 0; i < 10; ++i) {
      ret = ret * 573292817ULL + m_[i];
    }
    return ret;
  }

 private:
  std::array<uint64_t, 10> m_;
  int jama_count_ = 0;
  int size_ = 0;
  uint64_t hash_ = 0;
};
