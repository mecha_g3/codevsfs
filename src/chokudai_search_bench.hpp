#pragma once

#include <map>

#include "benchmark_misc.hpp"
#include "chokudai_search.hpp"

static BenchResult ImplChokudaiSearchBenchmark(
    const GameInfo &game_info, const TurnInfo &turn_info, int time_limit_ms,
    std::map<std::string, std::string> &args) {
  static const int BeamWidth = 6000;
  static const int BeamDepth = 60;

  const auto &packs = game_info.packs;

  BenchResult result;
  Board board = turn_info.m_board;

  int target_turn = 20;
  int turn_range = 0;
  bool use_good_pack = false;

  if (args.find("-target_turn") != args.end()) {
    target_turn = std::stoi(args["-target_turn"]);
  }

  if (args.find("-gpack") != args.end()) {
    use_good_pack = true;
    turn_range = std::stoi(args["-gpack"]);
  }

  using Search = ChokudaiSearch<BeamWidth, BeamDepth>;
  Search search(packs, board, turn_info.turn, turn_info.m_stock);

  auto eval_func = EvalFuncMyBest();
  eval_func.Init();

  if (use_good_pack) {
    GoodPackHolder gp_holder;
    gp_holder.Init(packs);
    int cnt = 0;
    for (int t = target_turn - turn_range; t < target_turn; ++t) {
      const auto pack = packs.Get(t);
      for (auto rot : gp_holder.Get(t)) {
        for (int pos = -2; pos <= 0; ++pos) {
          if (eval_func.AddTargetPack(pack, pos, rot)) {
            cnt++;
            break;
          }
        }
        for (int pos = kWidth - 1; kWidth - 3 <= pos; --pos) {
          if (eval_func.AddTargetPack(pack, pos, rot)) {
            cnt++;
            break;
          }
        }
      }
    }
    std::cerr << "cnt =" << cnt << std::endl;
    if (cnt == 0) {
      auto pack = packs.Get(target_turn - 1);
      eval_func.AddTargetPack(pack);
    }
  } else {
    auto pack = packs.Get(target_turn - 1);
    eval_func.AddTargetPack(pack);
  }

  search.Run(eval_func, target_turn, time_limit_ms);

  auto report = search.GetReports();
  if (report.empty()) {
    std::cerr << "empty report" << std::endl;
    return result;
  }

  auto best =
      *std::max_element(report.begin(), report.end(),
                        [](const Search::Report &a, const Search::Report &b) {
                          return a.score < b.score;
                        });
  result.chain = best.chain;
  result.score = best.score;

  for (const auto &r : report) {
    gv.NewTime();
    gvBoard(r.board);
    gv.Text(0, -2, 1.0, gv.ColorIndex(0), "Hash:%X", r.board.Hash());
  }

  for (const auto &r : best.log) {
    result.commands.push_back(r);
  }
  result.commands.push_back(best.cmd);

  search.ClearReports();

  return result;
}

static void RunChokudaiSearchBenchmark(
    const GameInfo &game_info, const TurnInfo &turn_info, int time_limit_ms,
    std::map<std::string, std::string> &args) {
  std::cout << "!start" << std::endl;
  auto start_time = std::chrono::high_resolution_clock::now();
  auto result =
      ImplChokudaiSearchBenchmark(game_info, turn_info, time_limit_ms, args);
  auto end_time = std::chrono::high_resolution_clock::now();
  auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(
                     end_time - start_time)
                     .count();

  std::cout << "!time_limit:" << time_limit_ms << std::endl;
  std::cout << "!time:" << (int)elapsed << std::endl;
  std::cout << "!score:" << result.score << std::endl;
  std::cout << "!chain:" << result.chain << std::endl;
  std::cout << "!turn:" << result.commands.size() << std::endl;
  std::cout << "!simulation:" << g_metrics.simulator_fall_count << std::endl;
  std::cout << "!chokudai:" << g_metrics.chokudai_loop_count << std::endl;
  board_pool.print_stat();
  g_metrics.Print();
  std::cout << "!end" << std::endl;
}
