#pragma once
#include <iostream>

#include "common.hpp"

static void RunSimulatorBenchmark(const PackMan &packs) {
  Simulator sim;
  Board board, dst;
  board.Clear();
  auto start_time = std::chrono::system_clock::now();
  for (int i = 0; i < 100000; ++i) {
    for (int t = 0; t < 300; ++t) {
      bool updated = false;
      auto pack = packs.Get(t);
      for (int rot = 0; rot < 4; ++rot) {
        auto fixed_pack = ConvertPack(pack, rot, 0);
        for (int pos = -2; pos < 10; ++pos) {
          if (sim.Fall(dst, board, fixed_pack.first, pos)) {
            std::swap(dst, board);
            updated = true;
          }
        }
      }
      if (!updated) break;
    }
  }
  auto end_time = std::chrono::system_clock::now();
  auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(
                     end_time - start_time)
                     .count();
  g_metrics.Print();
  std::cout << "total:" << elapsed << "[ms]" << std::endl;
  std::cout << g_metrics.simulator_fall_count / (elapsed / 1000.0)
            << "[sim/sec]" << std::endl;
}
