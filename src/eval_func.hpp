#pragma once
#include <map>
#include <random>
#include <sstream>

#include "common.hpp"
#include "mmlib.hpp"

struct EvalFuncExtraParam {
  int fall_pack_index = -1;
  double prev_evalue;
};

struct EvalFuncArg {
  EvalFuncArg(const EvalFuncExtraParam& ext, const int base_turn,
              const int turn, const int target_turn, const Simulator& sim,
              const PackMan& packs, const Board& prev, const Board& next,
              const Pack& pack, const int pos, const int prev_jama,
              const int next_jama)
      : ext(ext),
        base_turn(base_turn),
        turn(turn),
        target_turn(target_turn),
        sim(sim),
        packs(packs),
        prev(prev),
        next(next),
        pack(pack),
        pos(pos),
        prev_jama(prev_jama),
        next_jama(next_jama) {}
  const EvalFuncExtraParam& ext;
  const int base_turn;
  const int turn;
  const int target_turn;
  const Simulator& sim;
  const PackMan& packs;
  const Board& prev;
  const Board& next;
  const Pack& pack;
  const int pos;
  const int prev_jama;
  const int next_jama;
};

struct EvalFuncResult {
  double evalue;
  int chain;
  EvalFuncExtraParam ext;
  bool ignore;
};

struct EvalFuncMyBest {
  struct WeightVector {
    std::array<double, 10> v;

    double chain() const { return v[0]; }
    double tail_erase_count() const { return v[1]; }
    double avg_erase_block_per_chain() const { return v[2]; }
    double normal_block_count() const { return v[3]; }
    double jama_block_count() const { return v[4]; }
    double after_block_count() const { return v[5]; }
    double after_m_stock() const { return v[6]; }

    void Parse(std::string param) {
      std::stringstream ss(param);
      std::string p;
      int i = 0;
      double value = 0;
      while (std::getline(ss, p, ',')) {
        std::cerr << "v=" << p << std::endl;
        value = std::stod(p);
        v[i] = value;
        i++;
      }
    }
    void Print() {
      std::cerr << "=== parameter ===" << std::endl;
      std::cerr << "w_chain:" << chain() << std::endl;
      std::cerr << "w_tail_erase_count:" << tail_erase_count() << std::endl;
      std::cerr << "w_avg_erase_bock_per_chain:" << avg_erase_block_per_chain()
                << std::endl;
      std::cerr << "w_normal_block_count:" << normal_block_count() << std::endl;
      std::cerr << "w_jama_block_count:" << jama_block_count() << std::endl;
      std::cerr << "w_after_bock_count:" << after_block_count() << std::endl;
      std::cerr << "w_after_m_stock:" << after_m_stock() << std::endl;
    }
  };

  Board board;
  Simulator sim;
  WeightVector wv;
  mmlib::Vector<mmlib::Pair<Pack, int>, 100> check_pack_poses;
  std::mt19937 mt;
  std::uniform_real_distribution<double> rand_score;

  void Init() {
    board.Clear();
    check_pack_poses.clear();

    wv.v[0] = 100000.0;
    wv.v[1] = 10.0;
    wv.v[2] = -100.0;
    wv.v[3] = 1000.0;
    wv.v[4] = -0.0;
    wv.v[5] = -1.0;
    wv.v[6] = -0;
    rand_score = std::uniform_real_distribution<double>(0.0, 1.0);
  }

  void AddCheckPack(int pos, int num) {
    Pack pack;
    pack.Clear();
    pack.Set(0, 0, num);
    check_pack_poses.push_back(mmlib::Pair<Pack, int>(pack, 0));
  }

  bool AddTargetPack(const Pack& pack, int pos, int rot) {
    auto fixed_pack = ConvertPack(pack, rot, 0);
    auto lr = GetPackLR(fixed_pack.first);
    if (0 <= pos + lr.first && pos + lr.second < 10) {
      check_pack_poses.push_back(mmlib::Pair<Pack, int>(fixed_pack.first, pos));
      return true;
    }
    return false;
  }

  void AddTargetPack(const Pack& pack) {
    for (int rot = 0; rot < 4; ++rot) {
      for (int pos = -2; pos <= 0; ++pos) {
        if (AddTargetPack(pack, pos, rot)) {
          break;
        }
      }
      for (int pos = kWidth - 1; kWidth - 3 <= pos; --pos) {
        if (AddTargetPack(pack, pos, rot)) {
          break;
        }
      }
    }
  }

  double CalcScore(const Board& after, const EvalFuncArg& arg) {
    return 1000.0 + rand_score(mt) + wv.chain() * sim.last_chain() +
           wv.tail_erase_count() * sim.last_tail_erase_count() +
           wv.avg_erase_block_per_chain() *
               (sim.last_erase_block_count() / double(sim.last_chain() + 1)) +
           wv.normal_block_count() * arg.next.NormalBlockCount() +
           wv.jama_block_count() * arg.next.JamaBlockCount() +
           wv.after_block_count() * after.TotalBlockCount() +
           wv.after_m_stock() * arg.next_jama;
  }

  EvalFuncResult operator()(const EvalFuncArg& arg) {
    EvalFuncResult ret;
    ret.evalue = -1;
    ret.ignore = false;
    if (arg.prev_jama == 0 &&
        arg.next.TotalBlockCount() < arg.prev.TotalBlockCount()) {
      ret.ignore = true;
      return ret;
    }
    if (0 < arg.prev_jama &&
        arg.next.TotalBlockCount() < arg.prev.TotalBlockCount() + 5) {
      ret.ignore = true;
      return ret;
    }
    if (arg.ext.fall_pack_index == -1) {
      int best_idx = -1;
      for (int i = 0; i < check_pack_poses.size(); ++i) {
        const auto check = check_pack_poses[i];
        if (!sim.Fall(board, arg.next, check.first, check.second)) continue;
        const double score = CalcScore(board, arg);
        if (ret.evalue < score) {
          ret.evalue = score;
          ret.chain = sim.last_chain();
          if (sim.last_chain() >= 4) {
            best_idx = i;
          }
        }
      }
      if (best_idx != -1) {
        ret.ext.fall_pack_index = best_idx;
      }
    } else {
      const auto check = check_pack_poses[arg.ext.fall_pack_index];
      if (sim.Fall(board, arg.next, check.first, check.second)) {
        const double score = CalcScore(board, arg);
        ret.evalue = score;
        ret.chain = sim.last_chain();
        ret.ext.fall_pack_index = arg.ext.fall_pack_index;
      }
    }
    ret.ext.prev_evalue = ret.evalue;
    ret.ignore = arg.prev_jama == 0 && ret.evalue < arg.ext.prev_evalue;
    return ret;
  }
};
