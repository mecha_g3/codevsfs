#pragma once

#include <algorithm>
#include <array>
#include <cmath>

#include "game_const.hpp"
#include "immintrin.h"
#include "metrics.hpp"

struct BoardHead {
 public:
  int Get(int x, int y) const {
    assert(0 <= x && x < 10);
    assert(16 <= y && y < 20);
    return (m_[x] >> ((y - 16) << 2)) & 0x0F;
  }

  void Set(int x, int y, int v) {
    assert(0 <= x && x < 10);
    assert(16 <= y && y < 20);
    assert(0 <= v && v <= 11 && v != 10);
    const auto u = Get(x, y);
    if (u) size_--;
    if (v) size_++;
    m_[x] ^= (u ^ v) << ((y - 16) << 2);
  }

  void Clear() {
    m_.fill(0);
    size_ = 0;
  }

  int Get(int x) const { return m_[x]; }

  int BlockCount() const { return size_; }

 private:
  std::array<int, 10> m_;
  int size_ = 0;
};

static inline void PrintHead(const BoardHead &b, std::ostream &os) {
  os << "- - - - - - - - - - - - -" << std::endl;
  for (int y = 19; y >= 16; --y) {
    for (int x = -1; x <= kWidth; ++x) {
      os << std::setw(2);
      if (x == -1 || x == kWidth) {
        os << "|";
        continue;
      }
      int value = b.Get(x, y);
      if (value == 11)
        os << "#";
      else if (value == 0)
        os << " ";
      else
        os << value;
    }
    os << std::endl;
  }
}

class FastSimulator {
  struct TestPack {
    int Get(int x, int y) const { return (x == 0 && y == 0) ? value : 0; }
    int value;
  };

  class Shakutori {
   public:
    inline bool Push(int v) {
      q_ <<= 4;
      q_ |= v;
      sum_ += v;
      ++right_;
      ++cnt_;
      while (sum_ > 10) {
        sum_ -= 0x0F & (q_ >> (--cnt_ << 2));
      }
      return sum_ == 10;
    }

    inline void SeekLeft(int v) {
      q_ |= v << (cnt_++ << 2);
      sum_ += v;
    }

    void Reset(const int base = 0) {
      q_ = 0;
      sum_ = 0;
      cnt_ = 0;
      right_ = base;
    }

    void ResetWith(int right, int v) {
      q_ = v;
      sum_ = v;
      cnt_ = 1;
      right_ = right;
    }

    int sum() const { return sum_; }
    int left() const { return right_ - cnt_; }
    int right() const { return right_; }
    int cnt() const { return cnt_; }

   private:
    uint64_t q_ = 0;
    int sum_ = 0;
    int cnt_ = 0;
    int right_ = 0;
  };

 public:
  template <typename TyBoard, typename TyPack>
  bool Fall(TyBoard &dst, const TyBoard &base, const TyPack &pack, int pos) {
    const auto lr = GetPackLR(pack);
    if (!(pos + lr.first >= 0 && pos + lr.second < 10)) return false;
    return FallInternal(dst, base, pack, pos);
  }

  template <typename TyBoard>
  bool CheckFall(TyBoard &dst, const TyBoard &base, int pos, int num) {
    if (!(pos >= 0 && pos < 10)) return false;
    TestPack pack;
    pack.value = num;
    return FallInternal(dst, base, pack, pos);
  }

 private:
  template <typename TyBoard, typename TyPack>
  bool FallInternal(TyBoard &dst, const TyBoard &base, const TyPack &pack,
                    int pos) {
    g_metrics.simulator_fall_count++;
    dst = base;

    BoardHead head;
    head.Clear();

    uint32_t x_mask = 0;
    uint32_t y_mask = 0;
    uint32_t z_mask = 0;
    uint32_t w_mask = 0;
    last_chain_ = 0;
    last_erase_block_count_ = 0;
    last_tail_erase_count_ = 0;

    for (int x = 0; x < 3; ++x) {
      auto h = dst.Height(pos + x);
      for (int y = 2; y >= 0; --y) {
        const auto v = pack.Get(x, y);
        if (v) {
          if (h < 16) {
            dst.Set(pos + x, h, v);
          } else {
            head.Set(pos + x, h, v);
          }
          x_mask |= 1 << (pos + x);
          y_mask |= 1 << h;
          z_mask |= 1 << (h + pos + x);
          w_mask |= 1 << (h + 9 - (pos + x));
          h++;
        }
      }
    }

    int chain = 0;
    int turn_score = 0;
    int erase_count = 0;
    int erase_x_mask = 0;
    int erase_y_mask = 0;
    int erase_mark[20][10] = {0};
    Shakutori s;

    const auto mark = [&](int x, int y) {
      erase_mark[y][x] = chain + 1;
      erase_x_mask |= 1 << x;
      erase_y_mask |= 1 << y;
      erase_count++;
    };

    for (;;) {
      last_erase_x_mask_ = erase_x_mask;
      last_erase_y_mask_ = erase_y_mask;
      erase_x_mask = 0;
      erase_y_mask = 0;
      erase_count = 0;

      for (auto mask = x_mask; mask;) {
        const auto x = _tzcnt_u32(mask);
        mask = _blsr_u32(mask);

        s.Reset();
        for (auto value = dst.Get(x); value; value >>= 4) {
          const int v = value & 0x0F;
          if (!v) break;
          if (s.Push(v)) {
            const auto r = s.right();
            for (int i = s.left(); i < r; ++i) {
              mark(x, i);
            }
          }
        }
        for (auto value = head.Get(x); value; value >>= 4) {
          const int v = value & 0x0F;
          if (!v) break;
          if (s.Push(v)) {
            const auto r = s.right();
            for (int i = s.left(); i < r; ++i) {
              mark(x, i);
            }
          }
        }
      }

      const auto lx = _tzcnt_u32(x_mask);
      const auto ux = 32 - _lzcnt_u32(x_mask);

      for (auto mask = y_mask; mask;) {
        const auto y = _tzcnt_u32(mask);
        mask = _blsr_u32(mask);

        const auto iv = (y < 16) ? dst.Get(lx, y) : head.Get(lx, y);
        if (iv == 0) {
          s.Reset(lx + 1);
        } else {
          s.ResetWith(lx + 1, iv);
          for (int x = lx - 1; x >= 0; --x) {
            const int v = (y < 16) ? dst.Get(x, y) : head.Get(x, y);
            if (!v) break;
            s.SeekLeft(v);
            if (s.sum() == 10) {
              const auto r = s.right();
              for (int i = s.left(); i < r; ++i) {
                mark(i, y);
              }
              break;
            } else if (s.sum() > 10) {
              break;
            }
          }
        }

        for (int x = lx + 1; x < 10 && s.left() <= ux; ++x) {
          const int v = (y < 16) ? dst.Get(x, y) : head.Get(x, y);
          if (!v) {
            s.Reset(x + 1);
            continue;
          }
          if (s.Push(v)) {
            const auto r = s.right();
            for (int i = s.left(); i < r; ++i) {
              mark(i, y);
            }
          }
        }
      }

      for (auto mask = z_mask; mask;) {
        const auto a = _tzcnt_u32(mask);
        mask = _blsr_u32(mask);

        const int base_x = a < 20 ? 0 : a - 19;
        const int base_y = a < 20 ? a : 19;
        const int ly = base_y - (lx - base_x);

        if (ly < 0 || 19 < ly) continue;
        const auto iv = (ly < 16) ? dst.Get(lx, ly) : head.Get(lx, ly);
        if (iv == 0) {
          s.Reset(lx + 1);
        } else {
          s.ResetWith(lx + 1, iv);
          int x = lx - 1;
          int y = ly + 1;
          for (; y < 20 && x >= 0; --x, ++y) {
            const int v = (y < 16) ? dst.Get(x, y) : head.Get(x, y);
            if (!v) break;
            s.SeekLeft(v);
            if (s.sum() == 10) {
              const int k = s.cnt();
              for (int i = 0; i < k; ++i) {
                mark(x + i, y - i);
              }
              break;
            } else if (s.sum() > 10) {
              break;
            }
          }
        }

        int x = lx + 1;
        int y = ly - 1;
        for (; x < 10 && s.left() <= ux && y >= 0; ++x, --y) {
          const int v = (y < 16) ? dst.Get(x, y) : head.Get(x, y);
          if (!v) {
            s.Reset();
            continue;
          }
          if (s.Push(v)) {
            const int k = s.cnt();
            for (int i = 0; i < k; ++i) {
              mark(x - k + 1 + i, y + k - 1 - i);
            }
          }
        }
      }

      for (auto mask = w_mask; mask;) {
        const auto a = _tzcnt_u32(mask);
        mask = _blsr_u32(mask);

        int base_x = a < 10 ? 9 - a : 0;
        int base_y = a < 10 ? 0 : a - 9;

        if (base_x < lx) {
          const int ly = base_y + (lx - base_x);
          const auto iv = (ly < 16) ? dst.Get(lx, ly) : head.Get(lx, ly);
          if (iv == 0) {
            s.Reset(lx + 1);
          } else {
            s.ResetWith(lx + 1, iv);
            int x = lx - 1;
            int y = ly - 1;
            for (; y >= 0 && x >= 0; --x, --y) {
              const int v = (y < 16) ? dst.Get(x, y) : head.Get(x, y);
              if (!v) break;
              s.SeekLeft(v);
              if (s.sum() == 10) {
                const int k = s.cnt();
                for (int i = 0; i < k; ++i) {
                  mark(x + i, y + i);
                }
                break;
              } else if (s.sum() > 10) {
                break;
              }
            }
          }

          int x = lx + 1;
          int y = ly + 1;
          for (; x < 10 && s.left() <= ux && y < 20; ++x, ++y) {
            const int v = (y < 16) ? dst.Get(x, y) : head.Get(x, y);
            if (!v) {
              s.Reset();
              continue;
            }
            if (s.Push(v)) {
              const int k = s.cnt();
              for (int i = 0; i < k; ++i) {
                mark(x - k + 1 + i, y - k + 1 + i);
              }
            }
          }
        } else {
          int x = base_x;
          int y = base_y;
          s.Reset();
          for (; x < 10 && s.left() <= ux && y < 20; ++x, ++y) {
            const int v = (y < 16) ? dst.Get(x, y) : head.Get(x, y);
            if (!v) {
              s.Reset();
              continue;
            }
            if (s.Push(v)) {
              const int k = s.cnt();
              for (int i = 0; i < k; ++i) {
                mark(x - k + 1 + i, y - k + 1 + i);
              }
            }
          }
        }
      }

      if (!erase_count) break;
      last_tail_erase_count_ = erase_count;

      x_mask = 0;
      y_mask = 0;
      z_mask = 0;
      w_mask = 0;

      for (auto ex_mask = erase_x_mask; ex_mask;) {
        const auto x = _tzcnt_u32(ex_mask);
        ex_mask = _blsr_u32(ex_mask);

        uint64_t shrink_mask = 0;
        uint32_t min_y = 20;
        for (auto ey_mask = erase_y_mask; ey_mask;) {
          const auto y = _tzcnt_u32(ey_mask);
          ey_mask = _blsr_u32(ey_mask);

          if (erase_mark[y][x] == chain + 1) {
            min_y = std::min(min_y, y);
            x_mask |= 1 << x;
            last_erase_block_count_++;

            if (y < 16) {
              shrink_mask |= 0x0FULL << (4 * y);
            } else {
              head.Set(x, y, 0);
            }
          }
        }

        if (shrink_mask) {
          dst.Shrink(x, shrink_mask);
        }

        auto h = dst.Height(x);
        if (head.Get(x)) {
          for (int y = 16; y < 19; ++y) {
            auto v = head.Get(x, y);
            if (v) {
              if (h < 16) {
                dst.Set(x, h, v);
              } else {
                head.Set(x, h, v);
              }
              if (h != y) {
                head.Set(x, y, 0);
              }
              h++;
            }
          }
        }

        for (int y = min_y; y < h; ++y) {
          y_mask |= 1 << y;
          z_mask |= 1 << (y + x);
          w_mask |= 1 << (y + 9 - x);
        }
      }

      chain++;
      turn_score +=
          std::floor(std::pow(1.3, double(chain))) * (erase_count / 2);
    }

    if (head.BlockCount()) {
      return false;
    }

    last_chain_ = chain;
    last_score_ = turn_score;
    return true;
  }

 public:
  int last_chain() const { return last_chain_; }
  int last_score() const { return last_score_; }
  int last_tail_erase_count() const { return last_tail_erase_count_; }
  int last_erase_block_count() const { return last_erase_block_count_; }
  uint32_t last_erase_y_mask() const { return last_erase_y_mask_; }
  uint32_t last_erase_x_mask() const { return last_erase_x_mask_; }

 private:
  int last_chain_ = 0;
  int last_score_ = 0;
  int last_tail_erase_count_ = 0;
  int last_erase_block_count_ = 0;
  uint32_t last_erase_y_mask_ = 0;
  uint32_t last_erase_x_mask_ = 0;
};
