#pragma once

#include <deque>
#include "benchmark_misc.hpp"

static inline BenchResult ImplCounterBenchmark(
    const GameInfo &game_info, const TurnInfo &turn_info, int time_limit_ms,
    std::map<std::string, std::string> &args) {
  const auto &packs = game_info.packs;

  BenchResult result;
  Board board = turn_info.m_board;

  int block_count = turn_info.m_board.TotalBlockCount();
  int tmp_stock = turn_info.m_stock;
  int target_depth = 0;
  while (block_count < 160) {
    int turn = turn_info.turn + target_depth;
    auto pack = ConvertPack(game_info.packs.Get(turn), 0, tmp_stock);
    tmp_stock = pack.second;
    block_count += pack.first.Count();
    target_depth++;
  }

  target_depth = std::min(60, target_depth);
  target_depth = std::max(3, target_depth);
  std::cerr << "target_depth" << target_depth << std::endl;

  using Search = ChokudaiSearch<6000, 60>;
  Search search(packs, board, turn_info.turn, turn_info.m_stock);

  auto eval_func = EvalFuncMyBest();
  eval_func.Init();
  for (int pos = 0; pos < 10; pos++) {
    for (int num = 1; num < 10; ++num) {
      eval_func.AddCheckPack(pos, num);
    }
  }
  search.Run(eval_func, target_depth, time_limit_ms);

  auto report = search.GetReports();
  if (report.empty()) {
    result.score = -1;
    result.chain = -1;
    search.Clear();
    return result;
  }
  auto best =
      *std::max_element(report.begin(), report.end(),
                        [](const Search::Report &a, const Search::Report &b) {
                          return a.score < b.score;
                        });
  result.chain = best.chain;
  result.score = best.score;

  for (const auto &r : report) {
    gv.NewTime();
    gvBoard(r.board);
    gv.Text(0, -2, 1.0, gv.ColorIndex(0), "Hash:%X", r.board.Hash());
  }

  for (const auto &r : best.log) {
    result.commands.push_back(r);
  }
  result.commands.push_back(best.cmd);

  search.ClearReports();

  return result;
}

static void RunCounterBenchmark(const GameInfo &game_info,
                                const TurnInfo &default_turn_info,
                                int time_limit_ms,
                                std::map<std::string, std::string> &args) {
  std::cout << "!start" << std::endl;

  int attack_turn = 20;
  int attack_score = 700;
  if (args.find("-attack_turn") != args.end()) {
    attack_turn = std::stoi(args["-attack_turn"]);
  }
  if (args.find("-attack_score") != args.end()) {
    attack_turn = std::stoi(args["-attack_score"]);
  }

  auto start_time = std::chrono::system_clock::now();

  TurnInfo turn_info = default_turn_info;
  int turn = 0;
  Board board, tmp;
  board.Clear();
  Simulator sim;

  int total_m_damage = 0;
  int total_o_damage = 0;
  int total_score = 0;
  int max_chain = 0;

  std::deque<Command> reserved;

  for (int iteration = 0; iteration < 100; ++iteration) {
    if (reserved.empty()) {
      auto result =
          ImplCounterBenchmark(game_info, turn_info, time_limit_ms, args);
      for (auto cmd : result.commands) {
        reserved.push_back(cmd);
      }
      std::cerr << "expected:" << result.chain << std::endl;
    }
    auto cmd = reserved.front();
    reserved.pop_front();
    auto pack = ConvertPack(game_info.packs.Get(turn_info.turn), cmd.GetRot(),
                            turn_info.m_stock);
    if (!sim.Fall(tmp, board, pack.first, cmd.GetPos())) {
      std::cout << "invalid command" << std::endl;
      std::cout << cmd.GetPos() << " " << cmd.GetRot() << std::endl;
      break;
    }
    std::swap(tmp, board);
    turn_info.Print();
    PrintBoard(board, std::cerr);
    turn_info.turn++;
    turn_info.m_board = board;
    turn_info.m_stock = pack.second - sim.last_score() / 5;
    total_score += sim.last_score();
    max_chain = std::max(max_chain, sim.last_chain());

    if (turn_info.m_stock < 0) {
      std::cout << "!damage: turn:" << turn_info.turn
                << "stock:" << -turn_info.m_stock << std::endl;
      total_o_damage += (-turn_info.m_stock);
      turn_info.o_stock += -turn_info.m_stock;
      turn_info.m_stock = 0;
    }
    if (turn_info.turn % attack_turn == 0) {
      int damage = attack_score / 5;
      damage -= turn_info.o_stock;
      if (damage < 0) {
        turn_info.o_stock = -damage;
        damage = 0;
      }
      if (damage) {
        turn_info.o_stock = 0;
        turn_info.m_stock += damage;
        total_m_damage += damage;
        reserved.clear();
      }
    }
  }

  auto end_time = std::chrono::system_clock::now();
  auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(
                     end_time - start_time)
                     .count();

  std::cout << "!turn:" << turn_info.turn << std::endl;
  std::cout << "!score:" << total_score << std::endl;
  std::cout << "!chain:" << max_chain << std::endl;
  std::cout << "!m_damage:" << total_m_damage << std::endl;
  std::cout << "!o_damage:" << total_o_damage << std::endl;
  std::cout << "!simulation:" << g_metrics.simulator_fall_count << std::endl;
  std::cout << "!end" << std::endl;
}
