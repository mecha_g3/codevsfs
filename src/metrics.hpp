#pragma once

#include <iostream>

struct Metrics {
  int simulator_fall_count;
  int chokudai_loop_count;

  Metrics() : simulator_fall_count(0), chokudai_loop_count(0) {}

  void Print() {
    std::cerr << "====== METRICS ======" << std::endl;
    std::cerr << "simulator_fall_count:" << simulator_fall_count << std::endl;
    std::cerr << " chokudai_loop_count:" << chokudai_loop_count << std::endl;
    std::cerr << "=====================" << std::endl;
  }
};

static Metrics g_metrics;
