#pragma once
#include <cassert>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "common.hpp"

static inline void TestSimulator(
    const GameInfo& game_info, const std::vector<TurnInfo> turn_info_list,
    const std::vector<std::pair<Command, Command> > command_pair_list) {
  Simulator sim0;
  Simulator sim1;
  Board tmp0;
  Board tmp1;
  Board b0;
  Board b1;
  b0.Clear();
  b1.Clear();

  int jama0 = 0;
  int jama1 = 0;
  std::pair<Pack, int> pack0, pack1;

  for (int i = 0; i < command_pair_list.size(); ++i) {
    auto turn_info = turn_info_list[i];
    turn_info.Print();
    if (i) {
      ValidateBoardWithPack(turn_info_list[i - 1].m_board, b0,
                            turn_info.m_board, pack0.first);
      ValidateBoardWithPack(turn_info_list[i - 1].o_board, b1,
                            turn_info.o_board, pack1.first);
    }

    auto cmd0 = command_pair_list[i].first;
    auto cmd1 = command_pair_list[i].second;
    pack0 = ConvertPack(game_info.packs.Get(i), cmd0.GetRot(), jama0);
    pack1 = ConvertPack(game_info.packs.Get(i), cmd1.GetRot(), jama1);
    jama0 = pack0.second;
    jama1 = pack1.second;

    bool ok0 = sim0.Fall(tmp0, b0, pack0.first, cmd0.GetPos());
    bool ok1 = sim1.Fall(tmp1, b1, pack1.first, cmd1.GetPos());

    if (ok0 && ok1) {
      b0 = tmp0;
      b1 = tmp1;
      jama0 += sim1.last_score() / 5;
      jama1 += sim0.last_score() / 5;
      if (jama0 <= jama1) jama1 -= jama0, jama0 = 0;
      if (jama1 <= jama0) jama0 -= jama1, jama1 = 0;
      continue;
    }

    bool is_last_turn = (i + 1) == command_pair_list.size();
    assert(is_last_turn);
    break;
  }
}

static inline void TestSimulatorFromFile(const std::string& base_name) {
  std::ifstream in0s(base_name + "0.in");
  std::ifstream in1s(base_name + "1.in");
  std::ifstream out0s(base_name + "0.out");
  std::ifstream out1s(base_name + "1.out");

  assert(in0s.is_open());
  assert(in1s.is_open());
  assert(out0s.is_open());
  assert(out1s.is_open());

  std::vector<TurnInfo> turn_info0, turn_info1;
  std::vector<std::pair<Command, Command> > command_pair_list;

  int pos0, rot0, pos1, rot1;
  while (out0s >> pos0 >> rot0 && out1s >> pos1 >> rot1) {
    rot0 %= 4;  // 公式バグの為
    rot1 %= 4;
    Command cmd0;
    Command cmd1;
    cmd0.Set(pos0, rot0);
    cmd1.Set(pos1, rot1);
    command_pair_list.emplace_back(cmd0, cmd1);
  }

  auto game_info0 = ReadGameInfo(in0s);
  auto game_info1 = ReadGameInfo(in1s);

  for (int i = 0; i < command_pair_list.size(); ++i) {
    turn_info0.emplace_back(ReadTurnInfo(in0s));
    turn_info1.emplace_back(ReadTurnInfo(in1s));
  }

  TestSimulator(game_info0, turn_info0, command_pair_list);
}
