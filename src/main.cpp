#include "benchmark.hpp"
#include "chain_detector_test.hpp"
#include "simulator_test.hpp"

#include "glog.hpp"
#include "mecha_ai.hpp"

#include <signal.h>
#include <algorithm>
#include <bitset>
#include <fstream>
#include <functional>
#include <map>
#include <random>
#include <string>
#include <vector>

static const int wait_for_xcode_debug = 0;

int main(int argc, char **argv) {
  if (wait_for_xcode_debug) {
    raise(SIGSTOP);
  }

#ifdef ENABLE_GLOG
  google::InitGoogleLogging(argv[0]);
  google::InstallFailureSignalHandler();
#endif

  std::map<std::string, std::string> args;
  std::string key;
  for (int i = 1; i < argc; ++i) {
    if (argv[i][0] == '-') {
      key = (argv[i]);
      args[key] = "1";
    } else {
      std::cerr << key << " = " << argv[i] << std::endl;
      args[key] = argv[i];
    }
  }

  if (args.find("-test") != args.end()) {
    if (args["-simtest"] != "") {
      TestSimulatorFromFile(args["-simtest"]);
    }
    if (args["-cdtest"] != "") {
      TestChainDetector();
    }
    return 0;
  }

  if (args.find("-bench") != args.end()) {
    RunBenchmark(args);
    return 0;
  }

  MechaAI ai;
  ai.UseBattleLog();
  std::cout << ai.Name() << std::endl;
  while (ai.Think(std::cin, std::cout))
    ;

  return 0;
}
