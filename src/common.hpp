#pragma once

#include <cstring>
#include <iomanip>
#include <iostream>

#include "board.hpp"
#include "gv.hpp"
#include "metrics.hpp"
#include "mmlib.hpp"
#include "simulator.hpp"

using Board = FastBoard;
using Simulator = FastSimulator;

template <typename B>
static inline void gvBoard(const B &b) {
  for (int y = 0; y < 16; ++y) {
    for (int x = 0; x < kWidth; ++x) {
      if (b.Get(x, y)) {
        gv.Rect(
            x, 15 - y, 1, 1,
            gv.ColorIndex(2 * std::min(b.Get(x, y), 10 - b.Get(x, y)) + 20));
        gv.Text(x + 0.5, 15 - y + 0.5, 1, gv.ColorIndex(0), "%d", b.Get(x, y));
      }
    }
  }
}

template <typename B>
static inline void gvPack(const B &b) {
  for (int y = 0; y < 3; ++y) {
    for (int x = 0; x < 3; ++x) {
      gv.Rect(x, 15 - y, 1, 1, gv.ColorIndex(b.Get(x, y) + 10));
      gv.Text(x + 0.5, 15 - y + 0.5, 1, gv.ColorIndex(0), "%d", b.Get(x, y));
    }
  }
}

static inline void PrintBoard(const Board &b, std::ostream &os) {
  for (int y = 16; y >= -1; --y) {
    if (y == 16 || y == -1) {
      os << "- - - - - - - - - - - - -" << std::endl;
      continue;
    }
    for (int x = -1; x <= kWidth; ++x) {
      os << std::setw(2);
      if (x == -1 || x == kWidth) {
        os << "|";
        continue;
      }
      int value = b.Get(x, y);
      if (value == 11)
        os << "#";
      else if (value == 0)
        os << " ";
      else
        os << value;
    }
    os << std::endl;
  }
}

class Pack {
 public:
  int Get(int x, int y) const { return m_[y * kPackSize + x]; }
  void Set(int x, int y, int v) { m_[y * kPackSize + x] = v; }
  void Clear() { m_.fill(0); }
  int Count() const {
    int ret = 0;
    for (int i = 0; i < m_.size(); ++i) {
      if (m_[i]) ret++;
    }
    return ret;
  }

 private:
  std::array<uint8_t, kPackSize * kPackSize> m_;
};

static inline void PrintPack(const Pack &p, std::ostream &os) {
  for (int y = -1; y <= 3; ++y) {
    if (y == -1 || y == 3) {
      os << "- - - - - -" << std::endl;
      continue;
    }
    for (int x = -1; x <= 3; ++x) {
      os << std::setw(2);
      if (x == -1 || x == 3) {
        os << "|";
        continue;
      }
      int value = p.Get(x, y);
      if (value == 11)
        os << "#";
      else if (value == 0)
        os << " ";
      else
        os << value;
    }
    os << std::endl;
  }
}

static inline bool IsGoodPack(const Pack &p) {
  for (int y = 0; y < 2; ++y) {
    for (int x = 0; x < 3; ++x) {
      if (p.Get(x, y)) {
        int v = p.Get(x, y + 1);
        if (v == 0 || v == kWall) return false;
      }
    }
  }
  return true;
}

static inline std::vector<uint32_t> PackVector(const Pack &p) {
  std::vector<uint32_t> v;
  int bx, by;
  for (int y = 0; y < 3; ++y) {
    for (int x = 0; x < 3; ++x) {
      if (p.Get(x, y)) {
        if (v.empty()) {
          v.emplace_back(y << 20 | x << 16 | p.Get(x, y));
          bx = x;
          by = y;
        } else {
          v.emplace_back((y - by) << 20 | (x - bx) << 16 | p.Get(x, y));
        }
      }
    }
  }

  return v;
}

class PackMan {
 public:
  Pack Get(int turn) const { return m_[turn]; }
  void Set(int turn, Pack p) { m_[turn] = p; }

 private:
  std::array<Pack, kTurn> m_;
};

class Command {
 public:
  void Set(int pos, int rot) { m_ = ((pos + 2) << 2) | rot; }
  int GetPos() const { return (m_ >> 2) - 2; }
  int GetRot() const { return m_ & 0x03; }
  bool operator==(const Command &o) const { return m_ == o.m_; }
  bool operator!=(const Command &o) const { return m_ != o.m_; }

 private:
  int8_t m_;
};

template <typename B, typename C>
static inline void ValidateBoard(const B &b, const C &expected) {
  for (int y = 0; y < 16; ++y) {
    for (int x = 0; x < kWidth; ++x) {
      if (b.Get(x, y) != expected.Get(x, y)) {
        gv.NewTime();
        std::cerr << "expected" << std::endl;
        PrintBoard(expected, std::cerr);
        for (int y = 0; y < 16; ++y) {
          for (int x = 0; x < kWidth; ++x) {
            gv.Rect(x, 15 - y, 1, 1, gv.ColorIndex(expected.Get(x, y)));
            gv.Text(x + 0.5, 15 - y + 0.5, 0.2, gv.ColorIndex(0), "%d",
                    expected.Get(x, y));
          }
        }

        gv.NewTime();
        std::cerr << "actual" << std::endl;
        PrintBoard(b, std::cerr);
        for (int y = 0; y < 16; ++y) {
          for (int x = 0; x < kWidth; ++x) {
            gv.Rect(x, 15 - y, 1, 1, gv.ColorIndex(b.Get(x, y)));
            gv.Text(x + 0.5, 15 - y + 0.5, 0.2, gv.ColorIndex(0), "%d",
                    b.Get(x, y));
          }
        }
        assert(false);
      }
    }
  }
}

template <typename A, typename B, typename C, typename P>
static inline void ValidateBoardWithPack(const A &prev, const B &b,
                                         const C &expected, const P &pack) {
  for (int y = 0; y < 16; ++y) {
    for (int x = 0; x < kWidth; ++x) {
      if (b.Get(x, y) != expected.Get(x, y)) {
        gv.NewTime();
        gvBoard(prev);

        gv.NewTime();
        gvPack(pack);

        gv.NewTime();
        gvBoard(expected);

        gv.NewTime();
        gvBoard(b);

        std::cerr << "pack" << std::endl;
        PrintPack(pack, std::cerr);

        std::cerr << "prev" << std::endl;
        PrintBoard(prev, std::cerr);

        std::cerr << "expected" << std::endl;
        PrintBoard(expected, std::cerr);

        std::cerr << "actual" << std::endl;
        PrintBoard(b, std::cerr);

        assert(false);
      }
    }
  }
}

static inline std::pair<Pack, int> ConvertPack(const Pack &p, int rot,
                                               int jama) {
  Pack r;
  for (int y = 0; y < kPackSize; ++y) {
    for (int x = 0; x < kPackSize; ++x) {
      int n = p.Get(x, y);
      if (n == 0 && jama > 0) {
        jama--;
        n = kWall;
      }
      if (rot == 0) {
        r.Set(x, y, n);
      } else if (rot == 1) {
        r.Set(kPackSize - 1 - y, x, n);
      } else if (rot == 2) {
        r.Set(kPackSize - 1 - x, kPackSize - 1 - y, n);
      } else if (rot == 3) {
        r.Set(y, kPackSize - 1 - x, n);
      }
    }
  }
  return std::make_pair(r, jama);
}

struct GoodPackHolder {
 public:
  void Init(const PackMan &packs) {
    for (int t = 0; t < 300; ++t) {
      const auto pack = packs.Get(t);
      std::set<std::vector<uint32_t> > m;
      for (int rot = 0; rot < 4; ++rot) {
        const auto fixed = ConvertPack(pack, rot, 0).first;
        if (IsGoodPack(fixed)) {
          auto v = PackVector(fixed);
          if (m.find(v) == m.end()) {
            m.insert(v);
            holder_[t].push_back(rot);
          }
        }
      }
    }
  }

  const std::vector<int> &Get(int turn) { return holder_[turn]; }

 private:
  std::map<int, std::vector<int> > holder_;
};

static inline std::pair<int, int> GetPackLR(Pack p) {
  int pack_left = kPackSize;
  int pack_right = 0;
  for (int y = 0; y < kPackSize; ++y) {
    for (int x = 0; x < kPackSize; ++x) {
      if (p.Get(x, y)) {
        pack_left = std::min(pack_left, x);
        pack_right = std::max(pack_right, x);
      }
    }
  }
  return std::make_pair(pack_left, pack_right);
}

struct GameInfo {
  int W, H, T, S, N;
  PackMan packs;
};

struct TurnInfo {
  int turn;
  int remaining_ms;
  int m_stock;
  int o_stock;
  Board m_board;
  Board o_board;

  void Print() {
    std::cerr << "T:" << turn << " R:" << remaining_ms << " MS:" << m_stock
              << " OS:" << o_stock << std::endl;
  }
};

static inline GameInfo ReadGameInfo(std::istream &is) {
  std::string gomi;
  GameInfo g;

  int w, h, t, s, n;
  is >> w >> h >> t >> s >> n;
  assert(w == 10);
  assert(h == 16);
  assert(t == 3);
  assert(s == 10);
  assert(n == 500);

  g.W = w;
  g.H = h;
  g.T = t;
  g.S = s;
  g.N = n;

  for (int t = 0; t < 500; ++t) {
    Pack p;
    for (int y = 0; y < 3; ++y) {
      for (int x = 0; x < 3; ++x) {
        int v;
        is >> v;
        p.Set(x, y, v);
      }
    }
    g.packs.Set(t, p);
    is >> gomi;
    assert(gomi == "END");
  }
  return g;
}

static inline TurnInfo ReadTurnInfo(std::istream &is) {
  std::string gomi;
  int v;
  TurnInfo t;

  t.m_board.Clear();
  t.o_board.Clear();

  is >> t.turn;
  is >> t.remaining_ms;
  is >> t.m_stock;

  for (int y = 0; y < 16; ++y) {
    for (int x = 0; x < kWidth; ++x) {
      is >> v;
      assert(0 <= v && v <= 11);
      t.m_board.Set(x, 15 - y, v);
    }
  }
  is >> gomi;
  assert(gomi == "END");

  is >> t.o_stock;
  for (int y = 0; y < 16; ++y) {
    for (int x = 0; x < kWidth; ++x) {
      is >> v;
      assert(0 <= v && v <= 11);
      t.o_board.Set(x, 15 - y, v);
    }
  }
  is >> gomi;
  assert(gomi == "END");

  return t;
}

static mmlib::MemoryPool<Board, 1048560> board_pool;

static inline Board *Clone(const Board *const b) {
  auto dst = board_pool.get();
  std::memcpy(dst, b, sizeof(Board));
  return dst;
}

struct TurnMetrics {
  int m_empty_count = 0;
  int o_empty_count = 0;

  int m_block_count = 0;
  int o_block_count = 0;

  int m_jama_count = 0;
  int o_jama_count = 0;

  int m_score_potential = 0;
  int o_score_potential = 0;

  int m_stock_potential = 0;
  int o_stock_potential = 0;

  float m_fill_ratio = 0.0;
  float o_fill_ratio = 0.0;

  void Print() {
    std::cerr << "M: E" << m_empty_count << " B:" << m_block_count
              << " J:" << m_jama_count << " ScP:" << m_score_potential
              << " StP:" << m_stock_potential << " F%" << m_fill_ratio * 100
              << std::endl;
    std::cerr << "O: E" << o_empty_count << " B:" << o_block_count
              << " J:" << o_jama_count << " ScP:" << o_score_potential
              << " StP:" << o_stock_potential << " F%" << o_fill_ratio * 100
              << std::endl;
  }
};

static inline TurnMetrics CalcTurnMetrics(const GameInfo &game_info,
                                          const TurnInfo &turn_info) {
  TurnMetrics r;

  r.m_empty_count = turn_info.m_board.EmptyCount();
  r.o_empty_count = turn_info.o_board.EmptyCount();

  r.m_block_count = turn_info.m_board.NormalBlockCount();
  r.o_block_count = turn_info.o_board.NormalBlockCount();

  r.m_jama_count = turn_info.m_board.JamaBlockCount();
  r.o_jama_count = turn_info.o_board.JamaBlockCount();

  Simulator sim;
  Board dummy;

  for (int pos = 0; pos < kWidth; ++pos) {
    for (int num = 1; num < 10; ++num) {
      if (sim.CheckFall(dummy, turn_info.m_board, pos, num)) {
        r.m_score_potential = std::max(r.m_score_potential, sim.last_score());
      }
      if (sim.CheckFall(dummy, turn_info.o_board, pos, num)) {
        r.o_score_potential = std::max(r.o_score_potential, sim.last_score());
      }
    }
  }

  r.m_stock_potential = r.m_score_potential / 5;
  r.o_stock_potential = r.o_score_potential / 5;

  r.m_fill_ratio = float(turn_info.m_board.TotalBlockCount()) / (kWidth * 16);
  r.o_fill_ratio = float(turn_info.o_board.TotalBlockCount()) / (kWidth * 16);

  return r;
}
