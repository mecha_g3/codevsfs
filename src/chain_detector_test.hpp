#pragma once

#include "chain_detector.hpp"
#include "common.hpp"

static inline void TestChainDetector() {
  ChainDetector cd;
  /*
    Board board(
        "0000000000"
        "0002000000"
        "0034000000"
        "0011000800"
        "0026000560"
        "0057000110"
        "0096908520"
        "0875788660"
        "0768896650"
        "8153335384"
        "8411218438");
    Board board(
    "0000000300"
    "00##000600"
    "00##000200"
    "00##000700"
    "00##000500"
    "0084000100"
    "007##00800"
    "005##00400"
    "007##00400"
    "0081#08150"
    "0036#08380"
    "0088906960"
    "0158507760"
    "0#17807920"
    "0725602430"
    "0797222389");
    "0000000000"
  */
  /*
    Board board(
      "0000100000"
      "0000500000"
      "0000100020"
      "0000700070"
      "0000800020"
      "0000100020"
      "0000600010"
      "0000100010"
      "0000600070"
      "0000205090"
      "0005403080"
      "0008805047"
      "0001306218"
      "0002599763"
      "0001384929"
      "6052515336");
  */

  Board board(
      "9000000000"
      "#000000000"
      "8000000000"
      "#000010000"
      "6000#20000"
      "#000#90000"
      "5000#20000"
      "#00#41#000"
      "400#21#000"
      "#0#1616602"
      "3025832#9#"
      "#036894##9"
      "2116783##4"
      "#627584##7"
      "15998472#7"
      "#9434526#8");

  PrintBoard(board, std::cerr);
  std::cerr << "t" << board.TotalBlockCount() << std::endl;
  std::cerr << "j" << board.JamaBlockCount() << std::endl;
  std::cerr << "n" << board.NormalBlockCount() << std::endl;
  std::cerr << "h" << board.Height(0) << std::endl;
  std::cerr << "h" << board.Height(1) << std::endl;
  std::cerr << "h" << board.Height(2) << std::endl;
  std::cerr << "h" << board.Height(3) << std::endl;
  board.Shrink(0, 0xF0F0F0F0F0F0F0F0ULL);
  PrintBoard(board, std::cerr);
  std::cerr << "t" << board.TotalBlockCount() << std::endl;
  std::cerr << "j" << board.JamaBlockCount() << std::endl;
  std::cerr << "n" << board.NormalBlockCount() << std::endl;
  std::cerr << "h" << board.Height(0) << std::endl;
  std::cerr << "h" << board.Height(1) << std::endl;
  std::cerr << "h" << board.Height(2) << std::endl;
  std::cerr << "h" << board.Height(3) << std::endl;

  /*
  auto result = cd.Detect(board);
  std::cerr << result.chain << std::endl;
  std::cerr << result.pos << " " << result.num << std::endl;
  std::cerr << result.cpl_chain << std::endl;
  std::cerr << result.cpl_pos1 << " " << result.cpl_num1 << std::endl;
  std::cerr << result.cpl_pos2 << " " << result.cpl_num2 << std::endl;
  */
}
