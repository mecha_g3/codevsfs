#pragma once
#include <map>

#include "common.hpp"
#include "mecha_ai.hpp"

static BenchResult ImplMechaAIFirstAttackBench(
    const GameInfo &game_info, const TurnInfo &turn_info, int time_limit_ms,
    std::map<std::string, std::string> &args) {
  const auto &packs = game_info.packs;

  BenchResult result;

  MechaAI ai;
  ai.SetGameInfo(game_info);
  ai.UpdateTurnInfo(turn_info);
  ai.ThinkFirstAttack();
  ai.StopBackgroundIfRunning();
  auto buf = ai.GetBuffer();

  if (buf.empty()) {
    std::cerr << "empty report" << std::endl;
    return result;
  }

  for (const auto &r : buf) {
    result.commands.push_back(r);
  }

  int max_score = 0;
  int max_chain = 0;
  int m_stock = turn_info.m_stock;
  Board board = turn_info.m_board;
  Board tmp;
  Simulator sim;
  for (int i = 0; i < buf.size(); ++i) {
    auto pack = ConvertPack(game_info.packs.Get(turn_info.turn + i),
                            buf[i].GetRot(), m_stock);
    m_stock = pack.second;
    if (!sim.Fall(tmp, board, pack.first, buf[i].GetPos())) {
      std::cerr << "invalid command" << std::endl;
      break;
    }
    max_chain = std::max(max_chain, sim.last_chain());
    max_score = std::max(max_score, sim.last_score());
    std::swap(tmp, board);
  }
  result.score = max_score;
  result.chain = max_chain;
  return result;
}

static BenchResult ImplMechaAICounterBench(
    const GameInfo &game_info, const TurnInfo &turn_info, int time_limit_ms,
    std::map<std::string, std::string> &args) {
  const auto &packs = game_info.packs;

  BenchResult result;

  MechaAI ai;
  ai.SetGameInfo(game_info);
  ai.UpdateTurnInfo(turn_info);
  ai.ThinkCounter(19);
  ai.StopBackgroundIfRunning();
  auto buf = ai.GetBuffer();

  if (buf.empty()) {
    std::cerr << "empty report" << std::endl;
    return result;
  }

  for (const auto &r : buf) {
    result.commands.push_back(r);
  }

  int max_score = 0;
  int max_chain = 0;
  int m_stock = turn_info.m_stock;
  Board board = turn_info.m_board;
  Board tmp;
  Simulator sim;
  for (int i = 0; i < buf.size(); ++i) {
    auto pack = ConvertPack(game_info.packs.Get(turn_info.turn + i),
                            buf[i].GetRot(), m_stock);
    m_stock = pack.second;
    if (!sim.Fall(tmp, board, pack.first, buf[i].GetPos())) {
      std::cerr << "invalid command" << std::endl;
      break;
    }
    max_chain = std::max(max_chain, sim.last_chain());
    max_score = std::max(max_score, sim.last_score());
    std::swap(tmp, board);
  }
  result.score = max_score;
  result.chain = max_chain;
  return result;
}

static void RunMechaAIFirstAttackBench(
    const GameInfo &game_info, const TurnInfo &turn_info, int time_limit_ms,
    std::map<std::string, std::string> &args) {
  auto start_time = std::chrono::high_resolution_clock::now();
  auto result =
      ImplMechaAIFirstAttackBench(game_info, turn_info, time_limit_ms, args);
  auto end_time = std::chrono::high_resolution_clock::now();
  auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(
                     end_time - start_time)
                     .count();

  std::cout << "!time_limit:" << time_limit_ms << std::endl;
  std::cout << "!time:" << (int)elapsed << std::endl;
  std::cout << "!score:" << result.score << std::endl;
  std::cout << "!chain:" << result.chain << std::endl;
  std::cout << "!turn:" << result.commands.size() << std::endl;
  std::cout << "!simulation:" << g_metrics.simulator_fall_count << std::endl;
  std::cout << "!chokudai:" << g_metrics.chokudai_loop_count << std::endl;
  board_pool.print_stat();
  g_metrics.Print();
  std::cout << "!end" << std::endl;
}

static void RunMechaAICounterBench(const GameInfo &game_info,
                                   const TurnInfo &turn_info, int time_limit_ms,
                                   std::map<std::string, std::string> &args) {
  auto start_time = std::chrono::high_resolution_clock::now();
  auto result =
      ImplMechaAICounterBench(game_info, turn_info, time_limit_ms, args);
  auto end_time = std::chrono::high_resolution_clock::now();
  auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(
                     end_time - start_time)
                     .count();

  std::cout << "!m_stock:" << turn_info.m_stock << std::endl;
  std::cout << "!rev_stock:" << result.score / 5 << std::endl;

  std::cout << "!time_limit:" << time_limit_ms << std::endl;
  std::cout << "!time:" << (int)elapsed << std::endl;
  std::cout << "!score:" << result.score << std::endl;
  std::cout << "!chain:" << result.chain << std::endl;
  std::cout << "!turn:" << result.commands.size() << std::endl;
  std::cout << "!simulation:" << g_metrics.simulator_fall_count << std::endl;
  std::cout << "!chokudai:" << g_metrics.chokudai_loop_count << std::endl;
  board_pool.print_stat();
  g_metrics.Print();
  std::cout << "!end" << std::endl;
}
