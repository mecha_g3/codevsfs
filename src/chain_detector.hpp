#pragma once
#include "common.hpp"

class ChainDetector {
 public:
  struct Result {
    int chain;
    int pos;
    int num;
    int score;

    int cpl_chain;
    int cpl_pos1;
    int cpl_num1;
    int cpl_pos2;
    int cpl_num2;
    int cpl_score;
  };

  ChainDetector() = default;

  Result Detect(const Board& board) {
    Board tmp, tmp2;
    Result ret;
    ret.chain = 0;
    ret.cpl_chain = 0;
    uint16_t numbers;

    for (int pos = 0; pos < 10; ++pos) {
      for (int num = 1; num < 10; ++num) {
        if (sim_.CheckFall(tmp, board, pos, num)) {
          if (ret.chain < sim_.last_chain()) {
            ret.chain = sim_.last_chain();
            ret.score = sim_.last_score();
            ret.pos = pos;
            ret.num = num;
          }

          for (auto mask = sim_.last_erase_x_mask(); mask;) {
            int x = __builtin_ctz(mask);
            mask &= ~(1 << x);

            auto y_line = tmp.Get(x);
            int y;
            for (y = 0; y < 16; ++y) {
              if (!(y_line >> (y * 4))) {
                break;
              }
            }
            if (y == 16) continue;

            numbers = 0;
            const auto add_partner = [&numbers](int v) {
              if (v == 0 || v == 11) return;
              numbers |= 1 << (10 - v);
            };

            if (0 < x) {
              add_partner(tmp.Get(x - 1, y));
              if (0 < y) add_partner(tmp.Get(x - 1, y - 1));
              if (y < 15) add_partner(tmp.Get(x - 1, y + 1));
            }

            if (x < 9) {
              add_partner(tmp.Get(x + 1, y));
              if (0 < y) add_partner(tmp.Get(x + 1, y - 1));
              if (y < 15) add_partner(tmp.Get(x + 1, y + 1));
            }

            for (; numbers;) {
              int n = __builtin_ctz(numbers);
              numbers &= ~(1 << n);
              if (sim_.CheckFall(tmp, board, x, n) &&
                  sim_.CheckFall(tmp2, tmp, pos, num)) {
                if (ret.cpl_chain < sim_.last_chain()) {
                  ret.cpl_chain = sim_.last_chain();
                  ret.cpl_score = sim_.last_score();
                  ret.cpl_pos1 = x;
                  ret.cpl_num1 = n;
                  ret.cpl_pos2 = pos;
                  ret.cpl_num2 = num;
                }
              }
            }
          }
        }
      }
    }

    return ret;
  }

 private:
  Simulator sim_;
};
