#pragma once

#include <array>
#include <atomic>
#include <limits>
#include <thread>
#include "common.hpp"
#include "eval_func.hpp"
#include "mmlib.hpp"

template <int BeamWidth, int BeamDepth>
class ChokudaiSearch {
 public:
  using LogType = mmlib::Vector<Command, BeamDepth>;

  struct SearchNode {
    int chain = 0;
    int jama = 0;
    Board *board = nullptr;
    double score = 0;
    LogType log;
    EvalFuncExtraParam ext;
    bool operator<(const SearchNode &o) const { return score < o.score; }
  };

  struct Report {
    double score = 0;
    int chain = 0;
    int after_stock = 0;
    Board board;
    LogType log;
    Command cmd;
  };

  ChokudaiSearch(const ChokudaiSearch &) = delete;
  ChokudaiSearch(ChokudaiSearch &&) = delete;
  ChokudaiSearch &operator=(const ChokudaiSearch &) = delete;
  ChokudaiSearch() { Init(); }

  ChokudaiSearch(const PackMan &packs) : base_turn_(0), packs_(packs) {
    Init();
    SearchNode node;
    node.score = 0;
    node.board = board_pool.get();
    node.board->clear();
    m_[0].push(node);
  }

  ChokudaiSearch(const PackMan &packs, const Board &board, int turn, int jama)
      : base_turn_(turn), packs_(packs) {
    Init();
    SearchNode node;
    node.score = 0;
    node.board = Clone(&board);
    node.jama = jama;
    m_[0].push(node);
  }

  ~ChokudaiSearch() {
    Clear();
    ClearReports();
  }

  bool IsRunning() const { return running_; }

  void Reset(const PackMan &packs, const Board &board, int turn, int jama) {
    Clear();
    ClearReports();
    packs_ = packs;
    base_turn_ = turn;
    SearchNode node;
    node.score = 0;
    node.board = Clone(&board);
    node.jama = jama;
    m_[0].push(node);
    for (int i = 0; i < weak_hash_.size(); ++i) {
      weak_hash_[i].fill(0);
    }
  }

  void Clear() {
    for (int i = 0; i < m_.size(); ++i) {
      while (m_[i].size()) {
        board_pool.put(m_[i].min_element().board);
        m_[i].pop_min();
      }
    }
  }

  void ClearReports() {
    assert(!running_);
    for (int i = 0; i < report_.size(); ++i) {
      report_[i].score = 0;
    }
  }

  std::vector<Report> GetReports() const {
    assert(!running_);
    std::vector<Report> ret;
    for (int i = 0; i < report_.size(); ++i) {
      if (0.0 < report_[i].score) {
        ret.push_back(report_[i]);
      }
    }
    return ret;
  }

  void Determined(int depth, Command cmd) {
    std::cerr << "determined called" << depth << std::endl;

    for (int i = 0; i < m_.size(); ++i) {
      std::cerr << "depth:" << i << " " << m_[i].size() << "->";
      std::vector<SearchNode> nodes;
      while (m_[i].size()) {
        auto e = m_[i].min_element();
        m_[i].pop_min();
        if (e.log.size() < depth || e.log[depth] != cmd) {
          board_pool.put(e.board);
        } else {
          nodes.push_back(e);
        }
      }
      for (auto node : nodes) {
        m_[i].push(node);
      }
      std::cerr << m_[i].size() << std::endl;
    }

    std::cerr << "before report:" << std::endl;
    for (int i = depth; i < report_.size(); ++i) {
      std::cerr << "depth:" << i << " score:" << report_[i].score << std::endl;
    }

    assert(depth < report_.size());
    for (int i = 0; i < report_.size(); ++i) {
      if (report_[i].log.size() < depth) {
        report_[i].score = 0;
        continue;
      }
      if (report_[i].log.size() == depth) {
        if (report_[i].cmd != cmd) {
          report_[i].score = 0;
        }
        continue;
      }
      if (report_[i].log[depth] != cmd) {
        report_[i].score = 0;
        continue;
      }
    }

    std::cerr << "after report:" << std::endl;
    for (int i = depth; i < report_.size(); ++i) {
      std::cerr << "depth:" << i << " score:" << report_[i].score << std::endl;
    }
  }

  void StopBackground() {
    std::cerr << "call StopBackground" << std::endl;
    stop_flag_ = true;
    while (true) {
      if (!running_) break;
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
  }

  template <typename TyEvalFunc>
  void RunBackground(TyEvalFunc &eval_func, int target_depth = BeamDepth) {
    assert(!running_);
    running_ = true;
    stop_flag_ = false;
    auto th = std::thread([this, eval_func, target_depth] {
      TyEvalFunc eval_func_ = eval_func;
      std::cerr << "background chokudai start" << std::endl;
      while (!stop_flag_) {
        bool updated = this->RunInternal(eval_func_, target_depth, 10);
        if (!updated) {
          std::cerr << "updated is false" << std::endl;
          break;
        }
      }
      std::cerr << "background chokudai stop" << std::endl;
      running_ = false;
    });
    th.detach();
  }

  template <typename TyEvalFunc>
  void RunTimes(TyEvalFunc &eval_func, int target_depth = BeamDepth,
                int loop_count = 10) {
    assert(!running_);
    running_ = true;
    RunInternal(eval_func, target_depth, loop_count);
    running_ = false;
  }

  template <typename TyEvalFunc>
  void Run(TyEvalFunc &eval_func, int target_depth = BeamDepth,
           int timeout_ms = 2000) {
    assert(!running_);
    running_ = true;
    auto start_time = std::chrono::system_clock::now();
    while (true) {
      bool updated = RunInternal(eval_func, target_depth, 20);
      if (!updated) break;
      auto end_time = std::chrono::system_clock::now();
      auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(
                         end_time - start_time)
                         .count();
      if (timeout_ms < elapsed) break;
    }
    running_ = false;
  }

  void base_turn(int t) { return base_turn_ = t; }

  int base_turn() const { return base_turn_; }

 private:
  void Init() {
    weak_hash_.resize(BeamDepth);
    running_ = false;
    ClearReports();
    for (int i = 0; i < m_.size(); ++i) {
      m_[i].on_rankout(
          [](const SearchNode &node) { board_pool.put(node.board); });
    }
  }

  template <typename TyEvalFunc>
  bool RunInternal(TyEvalFunc &eval_func, int target_depth, int loop_count) {
    Simulator sim;
    SearchNode tmp_node;
    Board board;
    int final_turn = base_turn_ + target_depth - 1;
    bool updated = true;

    for (int loop = 0; updated && loop < loop_count; ++loop) {
      updated = false;
      g_metrics.chokudai_loop_count++;

      for (int depth = 0; depth < target_depth; ++depth) {
        const int turn = base_turn_ + depth;
        if (turn == kTurn) break;
        const auto pack = packs_.Get(turn);

        auto &prev = m_[depth];
        auto &next = m_[depth + 1];

        if (prev.empty()) continue;

        auto node = prev.max_element();
        prev.pop_max();

        assert(node.board != nullptr);
        const auto h = node.board->Hash();
        const auto h2 = uint16_t(h) ^ uint16_t(h >> 32);
        auto &hash_pool = weak_hash_[depth];
        if (hash_pool[h2] == h) {
          board_pool.put(node.board);
          continue;
        }
        hash_pool[h2] = h;
        updated = true;

        for (int rot = 0; rot < 4; ++rot) {
          const auto fixed_pack = ConvertPack(pack, rot, node.jama);
          for (int pos = -2; pos < kWidth; ++pos) {
            if (!sim.Fall(board, *node.board, fixed_pack.first, pos)) continue;

            const int jamadiff = sim.last_score() / 5;
            const int next_jama = std::max(0, fixed_pack.second - jamadiff);

            auto &best = report_[depth];
            if (best.score < sim.last_score()) {
              best.score = sim.last_score();
              best.chain = sim.last_chain();
              best.board = *node.board;
              best.after_stock = next_jama;
              best.log = node.log;
              best.cmd.Set(pos, rot);
            }

            const auto eval_arg =
                EvalFuncArg(node.ext, base_turn_, turn, final_turn, sim, packs_,
                            *node.board, board, fixed_pack.first, pos,
                            node.jama, next_jama);
            const auto e = eval_func(eval_arg);
            if (!e.ignore) {
              tmp_node.score = e.evalue;
              if (next.is_rankin(tmp_node)) {
                tmp_node.chain = e.chain;
                tmp_node.log = node.log;
                tmp_node.board = Clone(&board);
                tmp_node.jama = next_jama;
                tmp_node.ext = e.ext;
                Command cmd;
                cmd.Set(pos, rot);
                tmp_node.log.push_back(cmd);
                next.push(tmp_node);
              }
            }
          }
        }
        board_pool.put(node.board);
      }
    }
    return updated;
  }

  PackMan packs_;
  int base_turn_ = 0;
  std::atomic<bool> stop_flag_;
  std::atomic<bool> running_;
  std::vector<std::array<uint64_t, 1 << 16> > weak_hash_;
  std::array<Report, BeamDepth> report_;
  std::array<mmlib::Ranking<SearchNode, BeamWidth>, BeamDepth + 1> m_;
};
