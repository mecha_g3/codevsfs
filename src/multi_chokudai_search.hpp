#pragma once
#include <memory>

#include "chokudai_search.hpp"
#include "eval_func.hpp"

template <int BeamWidth, int BeamDepth>
class MultiChokudaiSearch {
 public:
  using Chokudai = ChokudaiSearch<BeamWidth, BeamDepth>;
  using Report = typename Chokudai::Report;

  void Reset(const PackMan& packs, const Board& board, int turn, int jama) {
    for (auto& ch : chokudai_list_) {
      ch->Clear();
      ch->ClearReports();
    }
    chokudai_list_.clear();
    packs_ = packs;
    base_turn_ = turn;
    base_board_ = board;
    base_jama_ = jama;
  }

  void Clear() {
    for (auto& ch : chokudai_list_) {
      ch->Clear();
    }
  }

  void ClearReports() {
    for (auto& ch : chokudai_list_) {
      ch->ClearReports();
    }
  }

  void Determined(int depth, Command cmd) {
    for (int i = 0; i < chokudai_list_.size(); ++i) {
      chokudai_list_[i].Determined(depth, cmd);
    }
  }

  template <typename TyEvalFunc>
  void Run(std::vector<TyEvalFunc>& eval_func_list,
           int target_depth = BeamDepth, int timeout_ms = 2000) {
    const auto n = eval_func_list.size();
    std::cerr << "run multi chokudai search. n = " << n << std::endl;

    for (int i = 0; i < n; ++i) {
      chokudai_list_.emplace_back(new Chokudai());
      chokudai_list_[i]->Reset(packs_, base_board_, base_turn_, base_jama_);
    }

    int single_time = double(timeout_ms) / n;
    for (int i = 0; i < n; ++i) {
      chokudai_list_[i]->Run(eval_func_list[i], target_depth, single_time);
    }
  }

  std::vector<typename Chokudai::Report> GetReports() {
    std::vector<typename Chokudai::Report> result;
    for (auto& ch : chokudai_list_) {
      for (const auto& r : ch->GetReports()) {
        result.push_back(r);  // slow.. fixme
      }
    }
    return result;
  }

 private:
  int base_turn_;
  int base_jama_;
  PackMan packs_;
  Board base_board_;
  std::vector<std::unique_ptr<Chokudai>> chokudai_list_;
};
