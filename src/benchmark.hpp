#pragma once

#include <fstream>
#include <map>
#include <string>

#include "chokudai_search_bench.hpp"
#include "counter_bench.hpp"
#include "mecha_ai_bench.hpp"
#include "multi_chokudai_search_bench.hpp"
#include "simulator_bench.hpp"

static void RunBenchmark(std::map<std::string, std::string> args) {
  std::ifstream ifs(args["-input"]);
  assert(ifs.is_open());
  std::cout << "!input:" << args["-input"] << std::endl;
  auto game_info = ReadGameInfo(ifs);
  auto turn_info = ReadTurnInfo(ifs);

  int time_limit = 2000;
  if (args.find("-limit") != args.end()) {
    time_limit = std::stoi(args["-limit"]);
  }

  if (args["-bench"] == "sim") {
    RunSimulatorBenchmark(game_info.packs);
  } else if (args["-bench"] == "chokudai") {
    RunChokudaiSearchBenchmark(game_info, turn_info, time_limit, args);
  } else if (args["-bench"] == "multi") {
    RunMultiChokudaiSearchBenchmark(game_info, turn_info, time_limit, args);
  } else if (args["-bench"] == "counter") {
    RunCounterBenchmark(game_info, turn_info, time_limit, args);
  } else if (args["-bench"] == "ai0") {
    RunMechaAIFirstAttackBench(game_info, turn_info, time_limit, args);
  } else if (args["-bench"] == "ai1") {
    RunMechaAICounterBench(game_info, turn_info, time_limit, args);
  } else {
    std::cerr << "no bench mode" << std::endl;
  }
  return;
}
